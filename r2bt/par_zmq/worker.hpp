/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef ZMQ_WORKER_9Q284U5Q4P9QTHQLT8Q9
#define ZMQ_WORKER_9Q284U5Q4P9QTHQLT8Q9

#include "defines.hpp"
#include "msg_types.hpp"

#define THIS_FILE get_filename(__FILE__)

template <unsigned DIM>
class worker
{
public:
    worker(zmq::context_t& c, r2bt::cell<DIM>* model)
        : context(c)
        , thread(new std::thread( &worker::do_work, std::ref(*this)))
        , the_cell( model->clone() )
        , sender(c, ZMQ_PAIR)
    {
    }

    worker() = delete;
    worker(const worker&) = delete;
    worker(worker&&) = delete;

    virtual ~worker()
    {
        LOG("worker (%p) closing sender socket (destructor)\n",this);
        sender.close();
        LOG("worker (%p) waiting for thread to finish (destructor)\n",this);
        thread->join();
        LOG("worker (%p) destroyed\n",this);
    }

    void init()
    {
        char socket_id[32];
        sprintf(socket_id, "inproc://#%p", this);
        sender.bind(socket_id);
        LOG_OBJ("bound to " << socket_id << " in worker::init" << std::endl);
    }

    void close() {
        sender.close();
        LOG_OBJ("closing sender socket in worker::close (" << this << ")" << std::endl);
    }

    void query(uintptr_t ptr, const std::array<double,DIM>& point, double sq_radius) const
    {
        /*
        zmq::socket_t& r = const_cast<zmq::socket_t&>(sender);

        zmq::message_t msg(sizeof(msg_data<DIM>));
        msg_data<DIM> *data = (msg_data<DIM>*) msg.data();
        memcpy( data->point, point, DIM*sizeof(double));
        data->sq_radius = sq_radius;
        data->address = ptr;

        LOG("worker %p sends msg sized: %u\n", this, msg.size());
        LOG("worker %p requests for point address %lx sq_radius %f point [ %f ... ]\n", this, data->address, data->sq_radius, data->point[0]);
        try
        {
            r.send(msg, 0);
        }
        catch ( zmq::error_t ex )
        {
            LOG("Fails socket send in %s:%u\n", THIS_FILE, __LINE__ );
            LOG("Error was %s\n", ex.what() );
        }
        LOG_OBJ("request to worker_cell finished\n");
        */
    }

    void do_work()
    {
        char socket_id[32];
        sprintf(socket_id, "inproc://#%p", this);
        zmq::socket_t receiver(context, ZMQ_PAIR);
        receiver.connect(socket_id);
        LOG_OBJ("connected to " << socket_id << " in zmq_worker::do_work" << std::endl);

        zmq::socket_t pusher(context, ZMQ_PUSH);
        pusher.connect(COLLECT_END_POINT);
        LOG_OBJ("connected to " << COLLECT_END_POINT << " in zmq_worker::do_work" << std::endl);

        zmq::socket_t pusher_to_manager(context, ZMQ_PUSH);
        pusher_to_manager.connect(MANAGER_PULLER_POINT);
        LOG_OBJ("connected to " << MANAGER_PULLER_POINT << " in zmq_worker::do_work" << std::endl);

        zmq::message_t msg;
        bool on_success = true;

        try
        {
            receiver.recv(&msg, 0);
        }
        catch ( zmq::error_t ex ) {
            LOG("worker %p throws exception %s in %s:%u\n", this, ex.what(), THIS_FILE, __LINE__ );
            on_success = false;
        }

        while ( on_success )
        {
            msg_data<DIM> data;
            memcpy(&data, msg.data(), sizeof(msg_data<DIM>));

            LOG("worker %p received msg sized: %u\n", this, msg.size());
            LOG("worker %p received: address: %lx sq_radius %f point [ %f ... ]\n", this, data.address, data.sq_radius, data.point[0]);

            LOG_OBJ("TODO Cambiar la interfaz de los resultado");
            /* TODO Cambiar la interfaz de los resultado
            query_result q_result = the_cell->query(data.point, data.sq_radius);*/

            try
            {
                /* TODO Cambiar la interfaz de los resultado
                zmq::message_t result_msg(sizeof(msg_header)+q_result.number_of*sizeof(uintptr_t));
                msg_header h;
                h.address = data.address;
                h.number_of = q_result.number_of;
                set_result_message(result_msg, h, q_result.neighbor_refs);

                LOG("worker %p sends result message sized: %u\n", this, result_msg.size());
                LOG("worker %p sends result message %u neighbors for %lx point\n", this, h.number_of, h.address);
                pusher.send(result_msg, 0);

                // push completed task to manager
                pusher_to_manager.send("D", 1, 0);

                // reuse msg
                msg.rebuild();
                receiver.recv(&msg, 0);
                */
            }
            catch ( zmq::error_t ex )
            {
                LOG("worker %p throws exception %s in %s:%u\n", this, ex.what(), THIS_FILE, __LINE__ );
                on_success = false;
            }
        }

        LOG("worker %p closing sockets do_work %s:%u\n", this, THIS_FILE, __LINE__ );

        pusher_to_manager.close();
        pusher.close();
        receiver.close();

        LOG("worker %p finished do_work %s:%u\n", this, THIS_FILE, __LINE__ );
    }

    zmq::context_t& context;
    std::unique_ptr<std::thread> thread;
    std::unique_ptr< r2bt::cell<DIM> > the_cell;
    zmq::socket_t sender;
};

#endif // ZMQ_WORKER_9Q284U5Q4P9QTHQLT8Q9
