/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_MESSAGE_TYPES_ZMQ_9850493HANFP043WUQ23
#define _R2BT_MESSAGE_TYPES_ZMQ_9850493HANFP043WUQ23

template <unsigned DIM>
struct msg_data
{
    uintptr_t address;
    double radius;
    double sq_radius;
    double point[DIM];
};

struct msg_header
{
    uintptr_t address;
    size_t number_of;
};

void set_result_message( zmq::message_t &msg, const msg_header& h, uintptr_t *results )
{
    msg_header *msg_ptr = (msg_header*)msg.data();
    *msg_ptr = h; // Copy header
    if ( h.number_of > 0 )
    {
        ++msg_ptr; // skip header
        memcpy(msg_ptr, results, h.number_of*sizeof(uintptr_t) ); // copy results
    }
}

void get_result_message( zmq::message_t &msg, msg_header& h, r2bt::query_result &v )
{
    const msg_header *msg_ptr = (const msg_header*)msg.data();
    h = *msg_ptr; // Copy header
    if ( h.number_of > 0 )
    {
        ++msg_ptr; // skip header
        const uintptr_t *results = (const uintptr_t *) msg_ptr;
        v.reserve( h.number_of );
        for ( size_t i = 0; i < h.number_of; ++i ) {
            v.push_back( results[i] );
        }
    }
}

#endif // _R2BT_MESSAGE_TYPES_ZMQ_9850493HANFP043WUQ23
