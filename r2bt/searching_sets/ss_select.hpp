/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_MULTI_CELL_SELECTION_JOEIRTJ034JTAP49J09W3T
#define _R2BT_MULTI_CELL_SELECTION_JOEIRTJ034JTAP49J09W3T

namespace r2bt {

template < unsigned DIM >
class multi_cell
{
public:
    multi_cell( int range_ = 0 )
        : range ( range_ )
        , inc ( exp2(range) )
    {
    }

    int range;
    double inc;

    std::forward_list< tessel<DIM> > select(const std::array<double, DIM>& coord, double radius) const
    {
        if ( DIM != 3 )
        {
            throw std::runtime_error("multi_cell selects is only implemented for DIM = 3");
        }

        std::forward_list< tessel<DIM> > result;
        const tessel<DIM> center = tessel<DIM>::get_tessel(coord, range);

        int r = 0; bool keep_going = true;

        while ( keep_going )
        {
            const std::forward_list < std::array<int, 3> >& increment_list = near_cells_3(r);

            for ( auto& increment : increment_list)
            {
                if ( filter(coord, radius, center, increment) )
                {
                    result.push_front( tessel<DIM>() );
                    tessel<DIM> &aux = result.front();
                    aux.coor[0] = center.coor[0] + increment[0];
                    aux.coor[1] = center.coor[1] + increment[1];
                    aux.coor[2] = center.coor[2] + increment[2];
                    LOG("Point [ %7.3f, %7.3f, %7.3f ]", coord[0], coord[1], coord[2]);
                    LOG_OBJ(" radius: " << radius << " looking in cell: " << aux << std::endl);
                }
            }

            keep_going = false;
            ++r;

            for ( unsigned i = 0; i < DIM; ++i )
            {
                keep_going |= coord[i] + radius > (center.coor[i] + r) * inc;
                keep_going |= coord[i] - radius < (center.coor[i] - r + 1) * inc;
            }

            LOG_OBJ(" keep going: " << keep_going << " r = " << r << std::endl);
        }
        return std::move(result);
    }

    static const std::forward_list < std::array<int, 3> >& near_cells_3(int r)
    {
        static std::vector < std::forward_list < std::array<int, 3> > > vector_list =
        {
            { // vector_list[0]
                { 0, 0, 0 }
            },

            { // vector_list[1]
                {-1,-1,-1 },{-1,-1,+1 },{-1,-1, 0 },{-1,+1,-1 },{-1,+1,+1 },{-1,+1, 0 },{-1, 0,-1 },{-1, 0,+1 },{-1, 0, 0 },
                {+1,-1,-1 },{+1,-1,+1 },{+1,-1, 0 },{+1,+1,-1 },{+1,+1,+1 },{+1,+1, 0 },{+1, 0,-1 },{+1, 0,+1 },{+1, 0, 0 },
                { 0,-1,-1 },{ 0,-1,+1 },{ 0,-1, 0 },{ 0,+1,-1 },{ 0,+1,+1 },{ 0,+1, 0 },{ 0, 0,-1 },{ 0, 0,+1 }
            },
        };

        if ( r <= 1 )
        {
            return vector_list[r];
        }
        else
        {
            if ( (unsigned)r >= vector_list.size() )
            {
                vector_list.push_back( std::forward_list < std::array<int, 3> >() );
            }

            int x, y, z;
            for ( x = -r; x <= r; ++x )
            {
                for ( y = -r; y <= r; ++y )
                {
                    for ( z = -r; z <= r; ++z )
                    {
                        if ( abs(x) >= r || abs(y) >= r || abs(z) >= r)
                        {
                            vector_list[r].insert_after( vector_list[r].before_begin(), { x, y, z });
                        }
                    }
                }
            }
            return vector_list[r];
        }
    }

    static const std::forward_list < std::array<int, DIM> >& near_cells(int i)
    {
        static std::forward_list < std::array<int, DIM> > list;

        throw std::runtime_error("multi_cell near_cells is only implemented for DIM = 3");

        return list;
    }

    bool filter(const std::array<double, DIM>& coord, double radius, const tessel<DIM>& center, const std::array<int, DIM> &increment) const
    {
        if ( radius < 0 )
        {
            return true;
        }

        int i = 0;
        for ( int delta_x : increment )
        {
            if ( !( delta_x == 0 || ( delta_x > 0 ?
                            coord[i] + radius > (center.coor[i] + delta_x) * inc :
                            coord[i] - radius < (center.coor[i] + delta_x + 1) * inc ) ) )
            {
                return false;
            }
            ++i;
        }
        return true;
    }


    // TODO Hacer el equivalente pero con K vecinos...

    /*
      Ya lo intentado y no me ha salido, el tema está en que el radio no puede ser el criterio a
      la hora de seleccionar celdas sencillamente porque no existe un radio conocido.
      Así pues, lo que hay que hacer es utilizar el número de puntos por celda, es decir, buscar en
      "espiral" para ir contando el número de puntos que suman las celdas, si la suma supera K entonces
      ya está completo. Naturalmente la primera frontera, es decir, para DIM = 3, las 27 celdas más cercanas
      son obligadas, excepto que en la primera (x=0, y=0, z=0; los incrementos del tessel) encontremos
      todos los vecinos con un radio más pequeño a la distancia entre el punto y el borde de la celda.

      Se podría hacer por rondas, primera ronda:

      { 0, 0, 0 } Es decir, la propia celda

      Si la distancia del último es mayor a la distancia entre el punto y el borde:

      combinaciones de -1, 0, 1 excluido el { 0, 0, 0 }, es decir:

      { -1, 0, 0 }
      { +1, 0, 0 }
      { 0, -1, 0 }
      { -1, -1, 0 }
      ... hasta las 26 combinaciones que tienen que salir

      Si la distancia del último es mayor a la distancia entre el punto y el borde:

      Siguiente ronda, combinaciones de -2, -1, 0, +1, +2 excluidas las anteriores

      ....
    */
};

} //namespace

#endif // _R2BT_MULTI_CELL_SELECTION_JOEIRTJ034JTAP49J09W3T
