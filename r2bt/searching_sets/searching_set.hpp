/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_NEIGHBORHOOD_GWTW453TYW465YW45
#define _R2BT_NEIGHBORHOOD_GWTW453TYW465YW45

namespace r2bt {

using multiquery_result_t = std::unordered_map< uintptr_t, query_result >;
using multi_K_query_result_t = std::unordered_map< uintptr_t, kquery_result >;

template <unsigned DIM>
class searching_set
{
public:
    static const unsigned dim = DIM;
    typedef point_adapter<DIM> PointAdapter;

    virtual ~searching_set() { }

    /*
        1 -> Create empty cells
        2 -> Compute origin of cell and call set_limits
        3 -> Call reserve method in cell to allocate memory
    */
    virtual void create_cells() = 0;

    /* Must call the build method for each cell in the searching set */
    virtual void build() = 0;

    /* Return range to be used in cells and tessel */
    virtual int get_range() = 0;

    /* Return number of cells */
    virtual size_t size() const = 0;

    /* Add one to point count when the point is in tessel cell */
    virtual void count_point( const tessel<DIM>& ) = 0;

    virtual void insert(const PointAdapter& p) = 0;

    virtual query_result query(const std::array<double, DIM>& coord, double radius, double sq_radius) const = 0;

    virtual multiquery_result_t multi_query(const std::vector<PointAdapter>& querying, double radius, double sq_radius) const
    {
        multiquery_result_t result( querying.size() );

        for ( const PointAdapter& a : querying )
        {
            result[ a.get_address() ] =  this->query( a.get_coordinates(), radius, sq_radius );
        }

        return std::move(result);
    }

    virtual multiquery_result_t multi_query(const std::vector<PointAdapter>& querying, const std::vector<double>& radius) const
    {
        multiquery_result_t result;
        auto ite = radius.begin();

        for ( const PointAdapter& a : querying )
        {
            result[ a.get_address() ] =  this->query( a.get_coordinates(), *ite, *ite * *ite );
            ++ite;
        }

        return std::move(result);
    }

    virtual kquery_result kquery(const std::array<double, DIM>& coord, size_t K) const = 0;

    virtual multi_K_query_result_t kquery(const std::vector<PointAdapter>& querying, size_t K) const
    {
        multi_K_query_result_t result;

        for ( const PointAdapter& a : querying )
        {
            result[ a.get_address() ] =  this->kquery( a.get_coordinates(), K );
        }

        return std::move( result );
    }

};

// h is the lenght of the cell
inline int length2range ( double h )
{
    if( h <= 0 )
    {
        throw std::runtime_error("Please insert positive lenght");
    }
    else
    {
        return static_cast<int>( round( log2(h) ) );
    }
}

} //namespace

#endif // _R2BT_NEIGHBORHOOD_GWTW453TYW465YW45
