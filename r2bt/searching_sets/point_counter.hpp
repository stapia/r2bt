/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_POINT_COUNTER_9875149HHF9Q24H51Q2R4918
#define _R2BT_POINT_COUNTER_9875149HHF9Q24H51Q2R4918

namespace r2bt {

template < unsigned DIM >
class point_counter : public searching_set<DIM>
{
public:
    void count_point(const tessel<DIM>& tessel_point) override
    {
        typename HashMapCounter::iterator found = counter.find(tessel_point);

        if ( found != counter.end() )
        {
            ++(found->second);
        }
        else
        {
            counter[ tessel_point ] = 1;
        }
    }

    size_t cell_size(const tessel<DIM>& tessel_point)
    {
        return counter[ tessel_point ];
    }

#ifdef R2BT_VERBOSE
    void cell_info()
    {
        std::cout << "Cells in this searching set are: " << this->counter.size() << std::endl;
        size_t points = 0;
        for ( auto &c : this->counter )
        {
            std::cout << c.first << " with " << c.second << " points" << std::endl;
            points += c.second;
        }
        std::cout << "Points stored: " << points << std::endl;
    }
#endif // R2BT_VERBOSE

    typedef std::unordered_map<tessel<DIM>, size_t> HashMapCounter;
    HashMapCounter counter;
};

} //namespace

#endif // _R2BT_POINT_COUNTER_9875149HHF9Q24H51Q2R4918
