/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_SS_KQUERY_3493JF498JAJF498J4RAF5J2958
#define _R2BT_SS_KQUERY_3493JF498JAJF498J4RAF5J2958

namespace r2bt {

template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multi_K_query_result_t kquery(Neighborhood* neigh, size_t K, Iterator first, Iterator last, FunctorCoordinates point2coordinates)
{
    return std::move( kquery( *neigh, K, first, last, point2coordinates) );
}

template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multi_K_query_result_t kquery(Neighborhood& neigh, size_t K, Iterator first, Iterator last, FunctorCoordinates point2coordinates)
{
    typedef typename std::iterator_traits<Iterator>::value_type QueryPoint;
    typedef typename std::iterator_traits<Iterator>::iterator_category iterator_tag;
    typedef typename std::remove_pointer<QueryPoint>::type QueryPointWitoutPointer;
    typedef point_adapter<Neighborhood::dim> Adapter;

    LOG_OBJ("Serial K querying for N points, K: " << K << "\n");

    multi_K_query_result_t result;
    Iterator ite;

    for ( ite = first ; ite != last; ++ite )
    {
        Adapter a;
        set_coordinates_and_tessel<Adapter,3>( a, neigh.get_range(), point2coordinates(*ite) );
        set_address<Adapter, QueryPointWitoutPointer>( a, *ite );
        compute_diff<Adapter, Iterator>( a, first, ite, iterator_tag{} );
        result[ a.get_address() ] =  neigh.kquery( a.get_coordinates(), K );
    }

    return std::move( result );
}

template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multi_K_query_result_t kquery(Neighborhood* neigh, size_t K, Iterator first, Iterator last, size_t size, FunctorCoordinates point2coordinates)
{
    return std::move( kquery( *neigh, K, first,  last, size, point2coordinates) );
}

template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multi_K_query_result_t kquery(Neighborhood& neigh, size_t K, Iterator first, Iterator last, size_t size, FunctorCoordinates point2coordinates)
{
    typedef typename std::iterator_traits<Iterator>::value_type QueryPoint;
    typedef typename std::iterator_traits<Iterator>::iterator_category iterator_tag;
    typedef typename std::remove_pointer<QueryPoint>::type QueryPointWitoutPointer;
    typedef point_adapter<Neighborhood::dim> Adapter;

    LOG_OBJ("Serial K querying for N points, K: " << K << "\n");

    multi_K_query_result_t result;
    std::vector<Adapter> vector_adapter;
    vector_adapter.reserve( size );

    for ( Iterator ite = first ; ite != last; ++ite )
    {
        Adapter a;
        set_coordinates_and_tessel<Adapter,3>( a, neigh.get_range(), point2coordinates(*ite) );
        set_address<Adapter, QueryPointWitoutPointer>( a, *ite );
        compute_diff<Adapter, Iterator>( a, first, ite, iterator_tag{} );
        vector_adapter.push_back( a );
    }

    return std::move( neigh.multi_K_query( vector_adapter, K) );
}

} //namespace

#endif // _R2BT_SS_KQUERY_3493JF498JAJF498J4RAF5J2958
