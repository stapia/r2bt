/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_NEIGHBORHOOD_REC_OQN2H3NO2NFP2P3JFONO2NOFNO2N3EWF23
#define _R2BT_NEIGHBORHOOD_REC_OQN2H3NO2NFP2P3JFONO2NOFNO2N3EWF23

#include "searching_set.hpp"
#include "point_counter.hpp"

namespace r2bt {

template < unsigned DIM >
class searching_set_rec : public point_counter<DIM>
{
public:
    searching_set_rec( double h, const std::string& name_ ) :
        range( length2range(h) ),
        length ( h ),
        name(name_)
    {
    }

    static searching_set<DIM>* create( double h, const std::string& name )
    {
        return new searching_set_rec(h, name);
    }

    virtual ~searching_set_rec() { }

    int get_range() override
    {
        return range;
    }

    virtual size_t size() const override
    {
        return domain.size();
    }

    void create_cells() override
    {
        for ( const auto& x : this->counter )
        {
            std::shared_ptr < cell<DIM> > c( factory::create<DIM>(name) );
            typename HashMap::value_type new_value(x.first, c);
            std::pair<typename HashMap::iterator,bool> ok = domain.insert( new_value );
            if ( ok.second )
            {
                typename HashMap::iterator new_value_ite = ok.first;
                //LOG_OBJ("Adding cell to searching set with tessel: " << new_value_ite->first << std::endl);
                new_value_ite->second->reserve( x.second );
                new_value_ite->second->set_limits( x.first.get_origin( range ), length );
            }
            else
            {
                throw std::runtime_error("Cannot insert cell in HashMap?");
            }
        }
    }

    typedef point_adapter<DIM> PointAdapter;

    void insert(const PointAdapter& p) override
    {
        std::array<double, DIM> coord = p.get_coordinates();

        typename HashMap::iterator found = domain.find( p.get_tessel() );

        if ( found != domain.end() )
        {
            //LOG("Adding %lx | %lu to ", p.get_address(), p.get_address());
            //LOG_OBJ( found->first << std::endl);
            found->second->add(coord, p.get_address());
        }
        else
        {
            throw std::runtime_error("Cell should be inserted and initialized previously, create_cells not being called?");
        }
    }

    void build() override
    {
        for ( auto& item : domain )
        {
            item.second->build();
        }
    }

    query_result query(const std::array<double, DIM>& coord, double radius, double sq_radius ) const override
    {
        if ( radius < 0 )
        {
            throw std::runtime_error("radius could not be less than zero");
        }

        query_result result;
        tessel<DIM> c = tessel<DIM>::get_tessel(coord,range);
        tessel<DIM> aux;

        long x[DIM];
        long r = (long)( radius / length ) + 1;

        if( DIM != 3)
        {
            tessel_query( coord, radius, sq_radius, c, aux, result, x, 0, r );
        }
        else
        {
            tessel_query_3( coord, radius, sq_radius, c, aux, result, x, r );
        }

        return std::move(result);
    }

    kquery_result kquery(const std::array<double, DIM>& coord, const size_t K) const override
    {
        kquery_result result;
        tessel<DIM> c = tessel<DIM>::get_tessel(coord,range);

        /* Initial query */
        typename HashMap::const_iterator found = domain.find(c);
        if ( found != domain.end() )
        {
            result = found->second->kquery(coord, K, -1.0 );
        }

        if ( DIM != 3 )
        {
            bool keep_going = true;
            long x[DIM], r = 0;
            double sq_radius = -1.0;
            tessel<DIM> aux;

            while ( keep_going )
            {
                r++;
                tessel_kquery( coord, sq_radius, c, aux, result, x, r, K, 0 );
                keep_going = false;
                if ( result.size() < K )
                {
                    keep_going = true;
                }
                else
                {
                    for ( unsigned i = 0; i < DIM; ++i )
                    {
                        keep_going |= coord[i] + sqrt(sq_radius) > (c.coor[i] + r) * length;
                        keep_going |= coord[i] - sqrt(sq_radius) < (c.coor[i] - r + 1) * length;
                    }
                }
            }
        }
        else
        {
            tessel_kquery_3( coord, c, result, K );
        }

        result.resize( K );
        return std::move(result);    }

    void cell_report(std::ostream& os)
    {
        os << domain.size();
    }

    int range;
    double length;
    const std::string name;

private:

    typedef std::shared_ptr< cell<DIM> > CellPtr;
    typedef std::unordered_map<tessel<DIM>, CellPtr> HashMap;

    HashMap domain;

    void tessel_query(const std::array<double, DIM>& coord, double radius, double sq_radius, const tessel<DIM>& c, tessel<DIM>& aux,
                      query_result& result, long x[DIM], unsigned i, const long r ) const
    {
        if( i < DIM )
        {
            for ( x[i]= -r; x[i] <= r; ++x[i]) {
                tessel_query( coord, radius, sq_radius, c, aux, result, x, i + 1, r );
            }
        }
        else
        {
            unsigned j;
            for( j = 0; j < DIM; ++j){
                aux.coor[j] = c.coor[j] + x[j];
            }

            typename HashMap::const_iterator found = domain.find(aux);
            if ( found != domain.end() )
            {
                query_result q_result = found->second->query(coord, radius, sq_radius);
                result.reserve(result.size()+q_result.size());
                result.insert(result.end(), q_result.begin(), q_result.end());
            }
        }
    }

    void tessel_query_3( const std::array<double, DIM>& coord, double radius, double sq_radius, const tessel<DIM>& c, tessel<DIM>& aux,
                         query_result& result, long x[DIM], const long r ) const
    {
        for ( x[0]= -r; x[0] <= r; ++x[0])
        {
            for ( x[1]= -r; x[1] <= r; ++x[1])
            {
                for ( x[2]= -r; x[2] <= r; ++x[2])
                {
                    aux.coor[0] = c.coor[0] + x[0];
                    aux.coor[1] = c.coor[1] + x[1];
                    aux.coor[2] = c.coor[2] + x[2];

                    typename HashMap::const_iterator found = domain.find(aux);
                    if ( found != domain.end() )
                    {
                        query_result q_result = found->second->query(coord, radius, sq_radius);
                        result.reserve(result.size()+q_result.size());
                        result.insert(result.end(), q_result.begin(), q_result.end());
                    }
                }
            }
        }
    }

    void tessel_kquery_3( const std::array<double, DIM>& coord, const tessel<DIM>& c, kquery_result& result, const size_t K ) const
    {
        bool keep_going = true;
        double sq_radius = -1.0;
        long x[DIM], r = 0;
        tessel<DIM> aux;
        //LOG_OBJ("Central tessel: " << c << std::endl);
        while ( keep_going )
        {
            r++;
            for ( x[0] = -r; x[0] <= r; ++x[0]  )
            {
                for ( x[1] = -r; x[1] <= r; ++x[1] )
                {
                    for ( x[2] = -r; x[2] <= r; ++x[2] )
                    {
                        if ( abs(x[0]) >= r || abs(x[1]) >= r || abs(x[2]) >= r )
                        {
                            aux.coor[0] = c.coor[0] + x[0];
                            aux.coor[1] = c.coor[1] + x[1];
                            aux.coor[2] = c.coor[2] + x[2];
                            typename HashMap::const_iterator found = domain.find(aux);
                            if ( found != domain.end() )
                            {
                                kquery_result q_result;

                                if ( result.size() < K )
                                {
                                    q_result = found->second->kquery( coord, K, std::numeric_limits<double>::infinity() );
                                }
                                else
                                {
                                    q_result = found->second->kquery(coord, K, sq_radius);
                                }
                                result.merge( q_result );
                                result.resize( K );
                                sq_radius = result.last_sq_distance();
                            }
                        }
                    }
                }
            }

            keep_going = false;
            if ( result.size() < K )
            {
                keep_going = true;
            }
            else // Copiado de Santiago:
            {
                for ( unsigned i = 0; i < DIM; ++i )
                {
                    keep_going |= coord[i] + sqrt(sq_radius) > (c.coor[i] + r) * length;
                    keep_going |= coord[i] - sqrt(sq_radius) < (c.coor[i] - r + 1) * length;
                }
            }
            //LOG_OBJ("Finished kquery (" << coord[0] << " " << coord[1] << " " << coord[2] << ") with sqradius: " << sq_radius << std::endl);
            //LOG_OBJ("Best point was at: " << result.begin()->first << std::endl);
        }
    }

    void tessel_kquery( const std::array<double, DIM>& coord,  double& sq_radius, const tessel<DIM>& c,  tessel<DIM>& aux,
                        kquery_result& result, long x[DIM], long& r, const size_t K, unsigned i ) const
    {
        if( i < DIM )
        {
            for ( x[i] = -r; x[i] <= r; ++x[i] )
            {
                tessel_kquery( coord, sq_radius, c, aux, result, x, r, K, i + 1 );
            }
        }
        else
        {
            bool not_visited = 0;
            unsigned j;
            for ( j = 0; j < DIM; ++j)
            {
                if (abs(x[j]) >= r)
                {
                    not_visited = 1;
                    break;
                }
            }

            if ( not_visited )
            {
                unsigned j;
                for( j = 0; j < DIM; ++j){
                    aux.coor[j] = c.coor[j] + x[j];
                }

                typename HashMap::const_iterator found = domain.find(aux);
                if ( found != domain.end() )
                {
                    kquery_result q_result;

                    if ( result.size() < K )
                    {
                        q_result = found->second->kquery( coord, K, std::numeric_limits<double>::infinity() );
                    }
                    else
                    {
                        q_result = found->second->kquery(coord, K, sq_radius);
                    }

                    result.merge(q_result);
                    result.resize( K );
                    sq_radius = result.last_sq_distance();
                }
            }
        }
    }

    // bool filter ( const tessel<DIM>& tc, double radius ) const
};

} //namespace

#endif // _R2BT_NEIGHBORHOOD_REC_OQN2H3NO2NFP2P3JFONO2NOFNO2N3EWF23
