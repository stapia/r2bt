/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_DISTANCE_AND_REF_091R49TJRQFRASEGEW
#define _R2BT_DISTANCE_AND_REF_091R49TJRQFRASEGEW

namespace r2bt {

struct distance_and_ref
{
    distance_and_ref( double _sq_distance = 0.0, uintptr_t _address = 0 )
        : sq_distance( _sq_distance )
        , address ( _address )
    {
    }
    double sq_distance;
    uintptr_t address;
};

inline bool operator<(const distance_and_ref& lhs, const distance_and_ref& rhs)
{
    return lhs.sq_distance < rhs.sq_distance;
}

inline bool operator==(const distance_and_ref& lhs, const distance_and_ref& rhs)
{
    return lhs.address == rhs.address && fabs(lhs.sq_distance - rhs.sq_distance) < 1e-10;
}

inline bool operator!=(const distance_and_ref& lhs, const distance_and_ref& rhs)
{
    return lhs.address != rhs.address || fabs(lhs.sq_distance - rhs.sq_distance) >= 1e-10;
}

} // namespace

#endif // _R2BT_DISTANCE_AND_REF_091R49TJRQFRASEGEW
