/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_STREE_CELL_NEIGHBORHOOD_WOIFNI23T2ONONF
#define _R2BT_STREE_CELL_NEIGHBORHOOD_WOIFNI23T2ONONF

#include "tree_cell.hpp"

/**
  Simple tree:
    This tree is built using a pivot point that is as simple as the
    point in the middle.
**/

namespace r2bt {

template < unsigned DIM >
class stree_cell : public tree_cell<DIM>
{
public:
    virtual ~stree_cell()
    {
        LOG_OBJ("Destroying stree_cell\n");
    }

    const std::string& name() override
    {
        static std::string the_name("stree");
        return the_name;
    }

    static cell<DIM>* create()
    {
        LOG_OBJ("Cloning stree_cell\n");
        return new stree_cell;
    }

private:

    /***
        TREE CONSTRUCTION
        Criteria: Equal values will be placed on the left ( <= )
    ***/

    /* See https://en.wikipedia.org/wiki/Dutch_national_flag_problem */
    int partition (int start, int end, unsigned axis, double pivot_value)
    {
        int left = start;
        int mid = start;
        int right = end;
        while ( mid <= right )
        {
            if( this->point_vector[mid][axis] < pivot_value )
            {
                if( left != mid ) this->swap(left, mid);
                ++left;
                ++mid;
            }
            else if ( this->point_vector[mid][axis] > pivot_value )
            {
                if( mid != right ) this->swap(mid, right);
                --right;
            }
            else {
                ++mid;
            }
        }

        if ( right < start ) {
            return start;
        }
        else {
            return right;
        }
    }

    /* Returns the index of the root element of the tree */
    int build_recursive(int start, int len, unsigned axis) override
    {
        if( len <= 0 ) return -1;

        int end = start + len - 1;
        int pivot = start + ( end - start )/2; // middle point

        pivot = partition(start, end, axis, this->point_vector[pivot][axis]);

        if ( ++axis >= DIM ) axis = 0;
        this->point_vector[pivot].left = build_recursive(start, pivot - start, axis);
        this->point_vector[pivot].right = build_recursive(pivot + 1, start + len - (pivot + 1), axis);
        return pivot;
    }
};

} // namespace

#endif // _R2BT_STREE_CELL_NEIGHBORHOOD_WOIFNI23T2ONONF
