/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_CELL_FACTORY_4U98UPW34WPGUW3PJW9AH9A77G38AGR
#define _R2BT_CELL_FACTORY_4U98UPW34WPGUW3PJW9AH9A77G38AGR

namespace r2bt {

template < unsigned DIM >
class cell_factory
{
public:
    cell_factory()
        : cells()
    {
        cells.insert ( Values("brute",   &brute_cell<DIM>::create) );
        cells.insert ( Values("stree",   &stree_cell<DIM>::create) );
        cells.insert ( Values("utree",   &utree_cell<DIM>::create) );
#ifdef OPENMP_WAS_FOUND
        cells.insert ( Values("omp",     &omp_cell<DIM>::create) );
#endif
#ifdef  ANN_WAS_FOUND
        cells.insert ( Values("ann",     &ann_cell<DIM>::create) );
#endif
        //cells.insert ( Values("at_tree", SmartPointer(new at_tree_cell<DIM>)) );
        //cells.insert ( Values("itree",   SmartPointer(new itree_cell<DIM>)) );
    }

    cell<DIM>* create(const std::string& name)
    {
        LOG_OBJ("Looking for cell name: " << name << "\n");
        auto found = cells.find(name);
        if ( found != cells.end() ) {
            LOG_OBJ("Creating cell: " << name << "\n");
            return found->second();
        }
        throw std::runtime_error("Cell name not found in cell factory");
        return nullptr;
    }

private:
    typedef cell<DIM>* (*CreatePointer) () ;
    typedef typename std::unordered_map<std::string, CreatePointer > Map;
    typedef typename Map::value_type Values;

    Map cells;
};

namespace factory {

template < unsigned DIM >
inline cell<DIM>* create(const std::string& name)
{
    static cell_factory<DIM> the_factory;
    LOG_OBJ("Trying to create cell " << name << "\n");
    return the_factory.create(name);
}

} //namespace

} //namespace

#endif // _R2BT_CELL_FACTORY_4U98UPW34WPGUW3PJW9AH9A77G38AGR
