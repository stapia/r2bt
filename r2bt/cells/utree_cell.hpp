/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef _R2BT_UTREE_CELL_NEIGHBORHOOD_20FJ9O2N292NDNJNIQNWD
#define _R2BT_UTREE_CELL_NEIGHBORHOOD_20FJ9O2N292NDNJNIQNWD

#include "tree_cell.hpp"

/**
    Unbalanced median tree
**/

namespace r2bt {

template < unsigned DIM >
class utree_cell : public tree_cell<DIM>
{
public:
    virtual ~utree_cell()
    {
        LOG_OBJ("Destroying u_tree_cell\n");
    }

    const std::string& name() override
    {
        static std::string the_name("utree");
        return the_name;
    }

    static cell<DIM>* create()
    {
        LOG_OBJ("Cloning utree_cell\n");
        return new utree_cell;
    }

private:
    /***
        TREE CONSTRUCTION
        Criteria: Equal values will be placed on the left
        Ex: 1 2 5 5 5 6 7
        median is 5
        -> left: 2 1 5 5 5
        -> right: 7 6
    ***/

    /* See https://en.wikipedia.org/wiki/Dutch_national_flag_problem */
    int unbalance_median (int start, int end, const unsigned axis, int median){
        int left = start;
        int mid = start;
        int right = end;
        const double median_value = this->point_vector[median][axis];
        while ( mid <= right )
        {
            if( this->point_vector[mid][axis] < median_value )
            {
                if( left != mid ) this->swap(left, mid);
                ++left;
                ++mid;
            }
            else if ( this->point_vector[mid][axis] > median_value )
            {
                if( mid != right ) this->swap(mid, right);
                --right;
            }
            else ++mid;
        }
        if( right < start ) return start;
        else return right;
    }

    /* See https://en.wikipedia.org/wiki/Quickselect */
    int select_median(int start, int end, const unsigned axis)
    {
        int i, pidx, median = start + (end-start)/2;
        double 	pivot;
        while(1){
            if ( start == end ) return start;
            pivot = this->point_vector[median][axis];

            //Partition algorithm
            this->swap(median, end); // Move pivot to end
            for ( pidx = i = start; i < end - 1; ++i )
            {
                if (  this->point_vector[i][axis] <= pivot )
                {
                    if( i != pidx ) this->swap(pidx, i);
                    ++pidx;
                }
            }
            this->swap(end, pidx); // Move pivot to its final place
            //End of partition
            if (  pidx == end  ) return pidx;
            if ( end  <  pidx  ) end = pidx - 1;
            else start = pidx + 1;
        }
    }

    /* Returns the index of the root element of the tree */
    int build_recursive(int start, int len, unsigned axis) override
    {
        if( len <= 0 ) return -1;
        int median;
        int end = start + len - 1;
        /* Find the median in order to build balanced trees*/
        median = select_median(start, end, axis);
        median = unbalance_median(start, end, axis, median);
        if ( ++axis >= DIM ) axis = 0;
        this->point_vector[median].left = build_recursive(start, median - start, axis);
        this->point_vector[median].right = build_recursive(median + 1, start + len - (median + 1), axis);
        return median;
    }
};

} // namespace

#endif // _R2BT_UTREE_CELL_NEIGHBORHOOD_20FJ9O2N292NDNJNIQNWD
