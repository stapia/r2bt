/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_TREE_CELL_39JT03OM3OMO3MGP3JOGN
#define _R2BT_TREE_CELL_39JT03OM3OMO3MGP3JOGN

namespace r2bt {

template < unsigned DIM >
class tree_cell : public cell<DIM>
{
public:

    virtual ~tree_cell() { }

    void set_limits(const std::array<double, DIM>& x0, double range)
    {
        origin = x0;
        length = range;
    }

    void reserve( size_t _size)
    {
        point_vector .reserve( _size );
        ref_vector   .reserve( _size );
    }

    void add(const std::array<double, DIM>& searching, uintptr_t ptr) override
    {
        tree_point<DIM> s( searching );
        point_vector.push_back( s );
        ref_vector  .push_back( ptr );
    }

    typedef tree_point<DIM> PointDim;
    typedef std::vector< PointDim > PointVector;
    typedef std::vector< uintptr_t > RefVector;

    PointVector point_vector;
    RefVector ref_vector;

    PointDim origin;
    double length = INFINITY ;

    /* Point to look neighbours, could be inside or outside the cell */
    query_result query(const std::array<double, DIM>& querying, double radius, double sq_radius) const override
    {
        query_result result;
        size_t len = point_vector.size();
        result.reserve( len / 2 ); // Only grows once

        unsigned axis = 0;
        query_recursive(root_index, querying, radius, sq_radius, axis, result);

        return std::move(result);
    }

    /* K Nearest Neighbour search */
    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {
        kquery_result result;
        if ( K > this->point_vector.size() )
        {
            unsigned index;
            for( index= 0; index < this->point_vector.size(); index++ )
            {
                double d = point_vector[index].norm( querying );
                result.emplace_front( d, ref_vector[index] );
            }
            result.sort();
        }
        else
        {
            if( target_sq_radius < 0 )
            {
                kquery_recursive ( result, root_index, querying, K, 0);
            }
            else
            {
                kquery_recursive ( result, root_index, querying, K, 0, target_sq_radius);
            }
        }
        return std::move( result );
    }

    void build() override
    {
        root_index = build_recursive ( 0, point_vector.size(), 0 );
    }

    int root_index = -1;

    inline void swap (unsigned idx1, unsigned idx2 )
    {
        point_vector[idx1].swap( point_vector[idx2] );

        uintptr_t tmpVoid;
        tmpVoid = ref_vector[idx1];
        ref_vector[idx1] = ref_vector[idx2];
        ref_vector[idx2] = tmpVoid;
    }

    virtual int build_recursive(int, int, unsigned) = 0;

    void query_recursive (int index, const std::array<double, DIM>& querying, double radius, double sq_radius, unsigned axis, query_result& result) const
    {
        if ( index == -1 ) return;

        double d, dx;
        d = point_vector[index].norm( querying );
        dx = point_vector[index][axis] - querying[axis];

        if ( d < sq_radius )
        {
            result.push_back(ref_vector[index]);
        }

        ++axis;
        axis = axis >= DIM ? 0 : axis;

        if ( dx > 0 )
        {
            query_recursive( point_vector[index].left, querying, radius, sq_radius, axis, result);
            if ( dx <= radius )
            {
                query_recursive( point_vector[index].right, querying, radius, sq_radius, axis, result);
            }
        }
        else
        {
            query_recursive( point_vector[index].right, querying, radius, sq_radius, axis, result);
            if ( - dx <= radius )
            {
                query_recursive( point_vector[index].left, querying, radius, sq_radius, axis, result);
            }
        }
    }

    void kquery_recursive (kquery_result& result, int index, const std::array<double, DIM>& querying, const int K, unsigned axis)
    {
        if ( index == -1 ) return;

        double d, dx, dx2;
        d = point_vector[index].norm( querying );

        if ( result.size() < (unsigned)K )
        {
            result.ordered_emplace(d, ref_vector[index]);
        }
        else if ( d == result.last_sq_distance() )
        {
            result.emplace_last( d, ref_vector[index] );
        }
        else if ( d < result.last_sq_distance() )
        {
            result.ordered_emplace(d, ref_vector[index]);
            result.resize(K);
        }

        dx = point_vector[index][axis] - querying[axis];
        dx2 = dx * dx;
        ++axis;
        if ( axis >= DIM ) axis = 0;
        kquery_recursive(result, dx > 0 ? point_vector[index].left : point_vector[index].right, querying, K, axis);
        if ( dx2 > result.last_sq_distance() && result.size() >= (unsigned)K ) return;
        kquery_recursive(result, dx > 0 ? point_vector[index].right : point_vector[index].left, querying, K, axis);
    }

    void kquery_recursive (kquery_result& result, int index, const std::array<double, DIM>& querying, const int K,
                           unsigned axis, const double sq_radius)
    {
        if ( index == -1 ) return;

        double d, dx, dx2;
        d = point_vector[index].norm( querying );

        if( d < sq_radius )
        {
            if ( result.size() < (unsigned) K )
            {
                result.ordered_emplace(d, ref_vector[index]);
            }
            else if ( d == result.last_sq_distance() )
            {
                result.emplace_last( d, ref_vector[index] );
            }
            else if ( d < result.last_sq_distance() )
            {
                result.ordered_emplace(d, ref_vector[index]);
                result.resize(K);
            }
        }
        dx = point_vector[index][axis] - querying[axis];
        dx2 = dx * dx;
        ++axis;
        if ( axis >= DIM ) axis = 0;
        kquery_recursive(result, dx > 0 ? point_vector[index].left : point_vector[index].right, querying, K, axis, sq_radius);
        if ( (dx2 > sq_radius) || ( (dx2 > result.last_sq_distance()) && (result.size() >= (unsigned)K)) ) return;
        kquery_recursive(result, dx > 0 ? point_vector[index].right : point_vector[index].left, querying, K, axis, sq_radius);
    }

#ifdef R2BT_VERBOSE
    void print_point (unsigned idx)
    {
        int i;
        std::cout << " [ ";
        for( i = 0; i < DIM; ++i ){
            std::cout << point_vector[idx][i] << " ";
        }
        std::cout << "] ";
    }

    void print_point (unsigned idx, unsigned axis)
    {
            std::cout << " [ ";
            std::cout << point_vector[idx][axis] << " ";
            std::cout << "] ";
    }

    void print_tree_recursive ( int index, int depth /*= 0 */)
    {
        if ( index == -1 ) return;
        print_point( index, depth%3 );
        std::cout << "\n";
        int i;
        if( point_vector[index].right != -1 ){
            for ( i = depth+1; i > 0; i-- ) std::cout << "\t";
            std::cout << "Right: ";
            print_tree_recursive ( point_vector[index].right, depth+1);
        }
        if( point_vector[index].left != -1 ){
            for ( i = depth+1; i > 0; i-- ) std::cout << "\t";
            std::cout << "Left: ";
            print_tree_recursive ( point_vector[index].left, depth+1);
        }
    }

    void check_tree_recursive(int index, unsigned axis)
    {
        if (index == -1 ) return;
        int left = point_vector[index].left;
        int right = point_vector[index].right;
        if(axis >= DIM) axis = 0;
        if(left != -1) {
            if(point_vector[index][axis] < point_vector[left][axis]){
                std::cout << "For axis " << axis << "point:\n";
                print_point(index, axis);
                std::cout << "is smaller than left:\n";
                print_point(left, axis);
                std::cout << std::endl;
            }
            check_tree_recursive(left, axis+1);
        }
        if(right != -1) {
            if(point_vector[index][axis] > point_vector[right][axis]){
                std::cout << "For axis " << axis << "point:\n";
                print_point(index, axis);
                std::cout << "is bigger than right:\n";
                print_point(right, axis);
                std::cout << std::endl;
            }
            else if(point_vector[index][axis] == point_vector[right][axis]){
                std::cout << "For axis " << axis << "point:\n";
                print_point(index, axis);
                std::cout << "is equal to right:\n";
                print_point(right, axis);
                std::cout << std::endl;
            }
            check_tree_recursive(right, axis+1);
        }
    }

    void check_point_recursive(int index, uintptr_t ptr, int& found )
    {
        if (index == -1 ) return;
        if (found) return;
        if( ref_vector[index] == ptr ) found = 1;
        int left = point_vector[index].left;
        int right = point_vector[index].right;
        if(left != -1) {
            check_point_recursive(left, ptr, found);
        }
        if(right != -1) {
            check_point_recursive(right, ptr, found);
        }
    }

    void test_median (const unsigned axis /*= 0*/)
    {
        for ( int i = 0; i < point_vector.size(); ++i)
        {
            print_point ( i, axis );
            print_point ( i );
            std::cout << "\n";
        }
        std::cout << "\n";
        int median = select_median(0, point_vector.size()-1, axis);
        std::cout << "\n";
        print_point(median); //
        std::cout << "\n";
        std::cout << "\n";
        for ( int i = 0; i < point_vector.size(); ++i)
        {
            print_point ( i, axis );
            print_point ( i );
            std::cout << "\n";
        }
        median = unbalance_median(0, point_vector.size()-1, axis, median);
        std::cout << "\n";
        std::cout << "\n";
        for ( int i = 0; i < point_vector.size(); ++i)
        {
            print_point ( i, axis );
            print_point ( i );
            std::cout << "\n";
        }
        std::cout << "\n";
        print_point( median );
        std::cout << "\n";
    }

    void print_tree ()
    {
        print_tree_recursive( root_index );
    }

    void check_tree()
    {
        check_tree_recursive(root_index, 0);
    }

    int check_point(uintptr_t ptr)
    {
        int found = 0;
        check_point_recursive(root_index, ptr, found);
        return found;
    }
#endif // R2BT_VERBOSE
};

} // namespace

#endif // _R2BT_TREE_CELL_39JT03OM3OMO3MGP3JOGN
