/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_K_QUERY_RESULT_q9384h9834y5q983
#define _R2BT_K_QUERY_RESULT_q9384h9834y5q983

namespace r2bt {

class kquery_result : public std::forward_list< distance_and_ref >
{
public:
    kquery_result()
        : std::forward_list< distance_and_ref > ()
        , _size ( 0 )
        , _last_sq_distance ( std::numeric_limits<double>::infinity() )
    {
    }

    void emplace_last(double sq_d, uintptr_t ref)
    {
        this->emplace_after( _last, sq_d, ref );
        ++_last;
        ++_size;
        _last_sq_distance = sq_d;
    }

    void ordered_emplace(double sq_d, uintptr_t ref)
    {
        if ( this->empty() )
        {
            this->emplace_front( sq_d, ref);
            _last = this->begin();
            _size = 1;
            _last_sq_distance = sq_d;
        }
        else
        {
            auto ite1 = this->before_begin();
            auto ite2 = this->begin();

            while ( ite2 != this->end() && ite2->sq_distance < sq_d )
            {
                ite1 = ite2;
                ++ite2;
            }

            this->emplace_after( ite1, sq_d, ref );

            if ( ite2 == this->end() )
            {
                _last = ++ite1;
                _last_sq_distance = sq_d;
            }
            ++_size;
        }
    }

    void resize(size_t K)
    {
        if ( ! this->empty() )
        {
            auto ite1 = this->begin();
            auto ite2 = ite1;
            size_t i = 1;

            ++ite2;
            // Beware i = 1 because we are already at first element
            for ( i = 1; ite2 != this->end() && i < K; ++i )
            {
                ++ite1; ++ite2;
            }

            while ( ite2 != this->end() && ite2->sq_distance == ite1->sq_distance )
            {
                ++ite1; ++ite2; ++i;
            }
            this->erase_after( ite1, this->end() );
            _last = ite1;
            _size = i;
            _last_sq_distance = ite1->sq_distance;
        }
        else
        {
            throw std::runtime_error("Don't call resize on empty list");
        }
    }

    inline size_t size() const
    {
        return _size;
    }

    inline double last_sq_distance() const
    {
        return _last_sq_distance;
    }

    inline kquery_result::iterator last() const
    {
        return _last;
    }

private:
    size_t _size;
    double _last_sq_distance;
    iterator _last;
};

#ifdef R2BT_VERBOSE

inline std::ostream& operator<< (std::ostream& out, const distance_and_ref& dr)
{
    out << " sq_distance: " << dr.sq_distance << ", address: " << dr.address;
    return out;
}

#endif // R2BT_VERBOSE

} // namespace

#endif // _R2BT_K_QUERY_RESULT_q9384h9834y5q983
