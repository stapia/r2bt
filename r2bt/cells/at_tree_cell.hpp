/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_AT_TREE_CELL_223527582957293578
#define _R2BT_AT_TREE_CELL_223527582957293578

#include "internal_points.hpp"
#include "cell.hpp"

namespace r2bt
{

template < unsigned DIM, unsigned SIZE = 4 >
class at_tree_cell : public cell<DIM>
{
public:

    using Point = internal_point<DIM>;

    at_tree_cell()
        : root( new leaf(this,0) )
    { }

    virtual ~at_tree_cell()
    {
        delete root;
    }

    const std::string& name() override
    {
        static std::string the_name("at_tree");
        return the_name;
    }

    cell<DIM>* clone() const override
    {
        LOG_OBJ("Cloning at_tree_cell\n");
        return new at_tree_cell();
    }

    void set_limits(const std::array<double, DIM>& x0, double range) override
    {
        origin = x0;
        length = range;
    }

    void reserve( size_t ) override { /* Skip: It is not neccesary */ }

    void add(const std::array<double, DIM>& searching, uintptr_t ptr) override
    {
        root = root->add(searching, ptr);
    }

    /* Point to look neighbours, could be inside or outside the cell */
    query_result query(const std::array<double, DIM>& querying, const double sq_radius) override
    {
        //std::cout << "Begin query" << std::endl;
        query_result result;
        root->query(result, querying, sq_radius, sqrt(sq_radius));
        //std::cout << "End query" << std::endl;
        return std::move( result );
    }

    /* K Nearest Neighbour search */
    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {
        kquery_result result;
        throw std::runtime_error("Not implemented yet");
        return std::move( result );
    }

    void build() override
    {
        //std::cout << "build cell not neccesary" << std::endl;
    }

private:
    enum NODE_TYPE { LEAF, BRANCH };

    struct node
    {
        virtual node* add( const Point& p, uintptr_t ptr) = 0;
        virtual void query(query_result& result, const std::array<double, DIM>& querying, const double sq_radius, const double radius) = 0;
        virtual NODE_TYPE node_type() const = 0;
        virtual ~node() {}
#ifdef R2BT_VERBOSE
        virtual void show(std::ostream& os) const = 0;
#endif
    };

    struct leaf;

    struct branch : public node
    {
        virtual void query(query_result& result, const std::array<double, DIM>& querying, const double sq_radius, const double radius) override
        {
            int side = querying[dimension] > split;
            sibling[side]->query(result, querying, sq_radius, radius);
            if ( fabs(querying[dimension] - split) < radius )
            {
                sibling[!side]->query(result, querying, sq_radius, radius);
            }
        }

        virtual node* add(const Point& p, uintptr_t ptr) override {
            int side = p[dimension] > split;
            sibling[ side ] = sibling[ side ]->add(p, ptr);
            return this;
        }

        virtual NODE_TYPE node_type() const override {
            return BRANCH;
        }

        branch(const at_tree_cell* p, unsigned depth_, leaf* sibling0 = nullptr, leaf* sibling1 = nullptr)
            : parent(p)
            , depth(depth_)
            , dimension( (depth_) % DIM)
            , split( parent->length / (2u << (depth_/DIM)) + parent->origin[dimension] )
        {
            sibling[0] = sibling0 == nullptr ? new leaf(parent, depth_+1) : sibling0;
            sibling[1] = sibling1 == nullptr ? new leaf(parent, depth_+1) : sibling1;
        }

        virtual ~branch() {
            delete sibling[0];
            delete sibling[1];
        }

        const at_tree_cell* parent;
        node *sibling[2];
        unsigned depth;
        unsigned dimension;
        double split;

#ifdef R2BT_VERBOSE
        virtual void show(std::ostream& os) const override {
            parent->do_indent(os, depth) << "Branch node, depth " << depth << std::endl;
            parent->do_indent(os, depth) << "Dimension " << dimension << " Split " << split << std::endl;
            parent->do_indent(os, depth) << "Left node: " << std::endl;
            sibling[0]->show(os);
            parent->do_indent(os, depth) << "Right node: " << std::endl;
            sibling[1]->show(os);
        }
#endif
    };

    struct leaf : public node
    {
        virtual void query(query_result& result, const std::array<double, DIM>& querying, const double sq_radius, const double) override
        {
            for ( unsigned i = 0; i < size; ++i )
            {
                if ( points[i].norm(querying) < sq_radius ) {
                    result.push_back( refs[i] );
                }
            }
        }

        virtual node* add( const Point& p, uintptr_t ptr) override {
            if ( size < SIZE ) {
                points[size] = p;
                refs[size] = ptr;
                ++size;
                return this;
            }
            else {
                std::array<Point, SIZE> aux_points = points;
                std::array<uintptr_t, SIZE> aux_refs = refs;
                size = 0;
                branch* new_branch = new branch(parent, depth, this, new leaf(parent, depth+1));
                ++depth;
                for ( unsigned i = 0; i < SIZE; ++i ) {
                    new_branch->add( aux_points[i], aux_refs[i] );
                }
                return new_branch;
            }
        }

        virtual NODE_TYPE node_type() const override {
            return LEAF;
        }

        leaf(const at_tree_cell* p, int depth_)
            : parent(p)
            , size(0)
            , depth(depth_)
        {}
        virtual ~leaf() {}

        const at_tree_cell* parent;
        unsigned size;
        unsigned depth;
        std::array<Point, SIZE> points;
        std::array<uintptr_t, SIZE> refs;

#ifdef R2BT_VERBOSE
        void show(std::ostream& os) const override {
            parent->do_indent(os, depth) << "Leaf node, Size: " << size << " depth: " << depth << std::endl;
            for ( unsigned i = 0; i < size; ++i ) {
                parent->do_indent(os, depth) << "Point: [ " << points[i][0] << ", " << points[i][1] << ", " << points[i][2] << "] "
                                             << "\t" << "Refs:" << refs[i] << std::endl;
            }
        }
#endif
    };

    node* root;

    Point origin;
    double length = INFINITY;

#ifdef R2BT_VERBOSE
    std::ostream& do_indent(std::ostream& os, unsigned depth) const {
        for ( unsigned i = 0; i < depth; ++i ) {
            os << "  ";
        }
        return os;
    }
public:
    void show(std::ostream& os) const {
        root->show(os);
    }
#endif
};

} //namespace

#endif // _R2BT_AT_TREE_CELL_223527582957293578
