/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef INCLUDE_R2BT_HPP_1958FQUA98FU42384H
#define INCLUDE_R2BT_HPP_1958FQUA98FU42384H

// Dependencies from standard libraries

#include <cstring>      // For memcpy
#include <cmath>        // For exp2 and log2 function (in side cell computation: 2^range)
#include <cstdint>      // For fix-sized integers

#include <memory>        // For smart pointers in the searching sets
#include <atomic>        // For atomic int to count messages between threads
#include <vector>        // For basic container in cells
#include <array>         // For getting point coordinates
#include <forward_list>  // For generating tessel of candidate cells
#include <algorithm>     // For sorting in brute_cells looking for k neighbors
#include <unordered_map> // For top level object data structure (searching_sets)
#include <utility>       // For pairs of references and distances

// Logging utils

#include "utils/logging.hpp"       // For debugging messages

// Cells and related to cells

#include "cells/distance_and_ref.hpp"    // results of K nearest neighbors queries depend on this
#include "cells/query_result.hpp"        // Result type for neighbors in a radius
#include "cells/kquery_result.hpp"       // Result type for nearest k neighbors
#include "cells/cell.hpp"                // Interface for all cells
#include "cells/internal_points.hpp"     // Internal points for cells, basically a std::array with a norm method
#include "cells/base_cell.hpp"           // A cell that uses a vector to store searching points
#include "cells/brute_cell.hpp"          // A base cell with a brute force query algorithm
#include "cells/tree_cell.hpp"           // Basic kd-tree, it has the query methods, but lacks the method to build the tree
#include "cells/utree_cell.hpp"          // Unbalanced median tree, it has the building method for the tree, query methods are already in the tree_cell
#include "cells/stree_cell.hpp"          // Unbalanced median tree, it has the building method for the tree, query methods are already in the tree_cell
//#include "cells/itree_cell.hpp"          // Another alternative for kd-trees. NOT FINISHED
//#include "cells/at_tree_cell.hpp"        // A cell that implements the tree as nodes in the heap. NOT FINISHED
//#include "cells/eigen_cell.hpp"          // Delete due to an error but anyway discarted

#include "cells/omp_cell.hpp"
#include "cells/ann_cell.hpp"

// Searching sets and related to searching sets

#include "cells/cell_factory.hpp"                // Creates cell type depending on number of points or fixed cell type
#include "searching_sets/bit_lattice.hpp"        // For an object that divides space and implement a hash
#include "searching_sets/tessel.hpp"             // For an object that divides space and implement a hash

#include "searching_sets/point_adapter.hpp"      // For providing get_address and get_coordinates
#include "searching_sets/searching_set.hpp"
#include "searching_sets/ss_helpers.hpp"  // Computes the diff between iterator when they are random access
#include "searching_sets/ss_select.hpp"         // For computing the tessels of the candidate cells to query
#include "searching_sets/ss_insert.hpp"         // Helper function to insert in a searching set
#include "searching_sets/ss_query.hpp"          // Helper function to query for neighbors in a radius in a searching set
#include "searching_sets/ss_kquery.hpp"         // Helper function to query for k nearest neighbors in a searching set
#include "searching_sets/searching_set_serial_3.hpp" // Searching set with a serial method to query the neigbors
#include "searching_sets/searching_set_rec.hpp"      // Generalized (in dimension) searching set with a serial method to query the neigbors
#include "searching_sets/searching_set_omp.hpp"      // using openmp

#include "r2bt/par_zmq/searching_set_zmq_3.hpp"      // Parallel searching set using ZMQ
#include "r2bt/par_tbb/searching_set_tbb_3.hpp"      // Parallel searching set using TBB
#include "r2bt/searching_sets/searching_set_one_cell.hpp" // Searching set wiht an unique cell (for debugging and performance reference)

#include "searching_sets/ss_factory.hpp"        // Factory object to create searching sets objects

#include "docs.hpp" // To document the interface

#endif //INCLUDE_R2BT_HPP_1958FQUA98FU42384H
