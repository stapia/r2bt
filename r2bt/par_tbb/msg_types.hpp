/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_MESSAGE_TYPES_TBB_2S9GWGJER8GJSAE9GJ
#define _R2BT_MESSAGE_TYPES_TBB_2S9GWGJER8GJSAE9GJ

namespace par_tbb
{

template <unsigned DIM>
struct query_data
{
    uintptr_t id;
    std::array<double,DIM> coord;
    tessel<DIM> center;
    double radius;
    double sq_radius;
};

template <unsigned DIM>
struct msg_data
{
    uintptr_t address;
    double radius;
    double sq_radius;
    std::array<double, DIM> point;
};

struct msg_result
{
    uintptr_t address;
    query_result neigh_refs;
};

struct msg_K_data_and_result
{
    enum { DIM = 3 };

    uintptr_t address;
    int r;
    std::forward_list < std::array<int, DIM> >::const_iterator ite, end;
    size_t K;
    double sq_radius;
    std::array<double, DIM> point;
    std::shared_ptr< kquery_result > result;
    multi_cell<DIM> mc;
    tessel<DIM> center;
    bool keep_going;
};

}

#endif // _R2BT_MESSAGE_TYPES_TBB_2S9GWGJER8GJSAE9GJ
