/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_NEIGHBORHOOD_TBB_0459UQ3AFMWM38RQ28R
#define _R2BT_NEIGHBORHOOD_TBB_0459UQ3AFMWM38RQ28R

#ifdef TBB_WAS_FOUND

#define THIS_FILE get_filename(__FILE__)

#include <tbb/flow_graph.h>
using namespace tbb::flow;

namespace r2bt {

#include "worker.hpp"
#include "collector.hpp"
#include "k_query_dispatcher.hpp"

class searching_set_tbb_3 : public point_counter<3>
{
public:
    enum { DIM = 3 };

    searching_set_tbb_3( double h, const std::string& name_ )
        : domain()
        , range( length2range(h) )
        , length ( h )
        , name(name_)
        , number_of_request(0)
        , g()
        , the_collector( g, number_of_request )
        , the_k_query_dispatcher(g, domain)
    {
    }

    static searching_set<DIM>* create( double h, const std::string& name )
    {
        return new searching_set_tbb_3(h, name);
    }

    virtual ~searching_set_tbb_3()
    {
    }

    int get_range() override
    {
        return range;
    }

    virtual size_t size() const override
    {
        return domain.size();
    }

    void create_cells() override
    {
        for ( const auto& x : this->counter )
        {
            LOG_OBJ("Adding to new cell\n");
            std::shared_ptr< par_tbb::worker<DIM> > ptr(new par_tbb::worker<DIM>(g, name ));
            typename HashMap::value_type new_value( x.first, ptr);
            std::pair<typename HashMap::iterator,bool> ok = domain.insert( new_value );
            if ( ok.second )
            {
                typename HashMap::iterator new_value_ite = ok.first;
                const tessel<DIM>& t = new_value_ite->first;
                LOG_OBJ("Adding cell to TBB searching set with tessel: " << t << std::endl);
                new_value_ite->second->the_cell->reserve( x.second );
                // Cell origin is the tessel * 2^range in each coordinate, the cell side is the 2^range
                new_value_ite->second->the_cell->set_limits( t.get_origin(range), exp2(range));
            }
            else
            {
                throw std::runtime_error("searching_set_tbb Cannot insert cell in HashMap?");
            }
        }
    }

    typedef point_adapter<DIM> PointAdapter;

    void insert(const PointAdapter& p) override
    {
        std::array<double, DIM> coord = p.get_coordinates();
        typename HashMap::iterator found = domain.find( p.get_tessel() );

        if ( found != domain.end() )
        {
            found->second->the_cell->add(coord, p.get_address());
            LOG("Added %p to existing cell ", p.get_address());
            LOG_OBJ( found->first << std::endl);
        }
        else
        {
            throw std::runtime_error("searching_set_tbb cell was not inserted in HashMap?");
        }
    }

    void build() override
    {
        for ( auto& item : domain )
        {
            item.second->the_cell->build();
            make_edge( item.second->node, the_collector.node );
            make_edge( item.second->node_for_k_query, the_k_query_dispatcher.node );
        }
    }

    void multi_k_query(uintptr_t id, const std::array<double,DIM>& coord, size_t K)
    {
        LOG_OBJ("multi_k_query" << std::endl);
        par_tbb::msg_K_data_and_result msg;

        msg.address = id;
        msg.r = 0;
        const std::forward_list < std::array<int, DIM> >& increment_list = multi_cell<DIM>::near_cells_3(msg.r);
        msg.ite = increment_list.cbegin();
        msg.end = increment_list.cend();
        msg.K = K;
        msg.sq_radius = std::numeric_limits<double>::infinity();
        msg.point = coord;
        msg.result =  std::make_shared<kquery_result>();
        msg.mc = multi_cell<DIM>(range);
        msg.center = tessel<DIM>::get_tessel(coord, range);
        msg.keep_going = true;

        the_k_query_dispatcher.node.try_put( msg );
    }

    void multi_query(uintptr_t id, const std::array<double,DIM>& coord, double radius, double sq_radius)
    {
        LOG("Begins multi querying, point: [ %f, ... ] radius: %f\n", coord[0], radius);

        const tessel<DIM> center = tessel<DIM>::get_tessel(coord, range);
        const double inc = exp2(range);
        const long r = (long)(radius / inc) + 1;

        long x,y,z;
        for ( x = -r; x <= r; ++x )
        {
            if ( x == 0 || ( x > 0 ?
                            coord[0] + radius > (center.coor[0] + x) * inc :
                            coord[0] - radius < (center.coor[0] + x + 1) * inc ) )
            {
                for ( y = -r; y <= r; ++y )
                {
                    if ( y == 0 || ( y > 0 ?
                                    coord[1] + radius > (center.coor[1] + y) * inc :
                                    coord[1] - radius < (center.coor[1] + y + 1) * inc ) )
                    {
                        for ( z = -r; z <= r; ++z )
                        {
                            if ( z == 0 || ( z > 0 ?
                                            coord[2] + radius > (center.coor[2] + z) * inc :
                                            coord[2] - radius < (center.coor[2] + z + 1) * inc ) )
                            {
                                tessel<DIM> aux;
                                aux.coor[0] = center.coor[0] + x;
                                aux.coor[1] = center.coor[1] + y;
                                aux.coor[2] = center.coor[2] + z;
                                LOG("Point [ %7.3f, %7.3f, %7.3f ]", coord[0], coord[1], coord[2]);
                                LOG_OBJ(" radius: " << radius << " looking in cell: " << aux << std::endl);

                                typename HashMap::const_iterator found = domain.find(aux);
                                if ( found != domain.end() )
                                {
                                    ++number_of_request;
                                    LOG_OBJ("multi_query: number of request = " << number_of_request << std::endl);
                                    par_tbb::msg_data<DIM> msg;
                                    msg.address = id;
                                    msg.point = coord;
                                    msg.radius = radius;
                                    msg.sq_radius = sq_radius;
                                    found->second->node.try_put( msg );
                                }
                            }
                        }
                    }
                }
            }
        }
        LOG("Ended multi querying, point: [ %f, ... ] radius: %f\n", coord[0], radius);
    }

    query_result query(const std::array<double, DIM>& /*p*/, double /*radius*/, double sq_radius) const override
    {
        throw std::runtime_error("Individual query not implemented...");
        query_result result;
        return std::move(result);
    }

    virtual multiquery_result_t multi_query(const std::vector<PointAdapter>& querying, const std::vector<double>& radius) const override
    {
        multiquery_result_t result( querying.size() );
        auto ite = radius.begin();

        /* Cast away const (implementation detail) should not be neccesary... */
        searching_set_tbb_3 *neigh = const_cast<searching_set_tbb_3*>(this);
        for ( const PointAdapter& a: querying )
        {
            neigh->multi_query( a.get_address(), a.get_coordinates(), *ite, *ite * *ite );
            ++ite;
        }

        /* Wait until result is completed (graph waits) */
        neigh->g.wait_for_all();

        return std::move( neigh->the_collector.multi_collect() );
    }

    multiquery_result_t multi_query(const std::vector<PointAdapter>& querying, double radius, double sq_radius) const override
    {
        searching_set_tbb_3 *neigh = const_cast<searching_set_tbb_3*>(this);
        for ( const PointAdapter& a: querying )
        {
            neigh->multi_query( a.get_address(), a.get_coordinates(), radius, sq_radius);
        }

        /* Wait until result is completed (graph waits) */
        neigh->g.wait_for_all();

        return std::move( neigh->the_collector.multi_collect() );
    }

    kquery_result kquery(const std::array<double, DIM>& /*p*/, size_t /*K*/) const override
    {
        throw std::runtime_error("Individual kquery not implemented...");
        kquery_result result;
        return std::move(result);
    }

    typedef std::unordered_map<tessel<DIM>, std::shared_ptr<par_tbb::worker<DIM> > > HashMap;
    HashMap domain;
    int range;
    double length;
    const std::string name;

    std::atomic_int number_of_request;
    graph g;
    par_tbb::collector the_collector;
    par_tbb::k_query_dispatcher the_k_query_dispatcher;
};

} //namespace

#endif // TBB_WAS_FOUND

#endif // _R2BT_NEIGHBORHOOD_TBB_0459UQ3AFMWM38RQ28R
