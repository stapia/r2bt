/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_COLLECTOR_TBB_QW0AJERTG0348U
#define _R2BT_COLLECTOR_TBB_QW0AJERTG0348U

#define THIS_FILE get_filename(__FILE__)

namespace par_tbb {

class collector {
public:
    collector( graph& g, std::atomic_int& nr  )
        : number_of_request (nr)
        , result()
        , node(g, 1, do_work(result, number_of_request))
    {
    }

    multiquery_result_t multi_collect()
    {
        return std::move(result);
    }

    struct do_work
    {
        do_work(multiquery_result_t& r, std::atomic_int& nr)
            : result( r )
            , number_of_request (nr)
        {
        }

        continue_msg operator() (const par_tbb::msg_result &msg) const
        {
            LOG("collector received result message %u neighbors for %lx point\n", msg.neigh_refs.size(), msg.address);

            if ( msg.neigh_refs.size() > 0 )
            {
                typename multiquery_result_t::iterator found = result.find(msg.address);
                if ( found != result.end() )
                {
                    LOG("Add result to existing point %p\n", msg.address);
                    query_result& v = found->second;
                    v.reserve(v.size()+msg.neigh_refs.size());
                    v.insert( v.end(), msg.neigh_refs.begin(), msg.neigh_refs.end() );
                }
                else
                {
                    LOG("Add result to new point %p\n", msg.address);
                    result[msg.address] = std::move(msg.neigh_refs);
                }
            }
            --number_of_request;
            LOG_OBJ("Collector: number of request = " << number_of_request << std::endl);

            return continue_msg();
        }

        multiquery_result_t& result;
        std::atomic_int& number_of_request;
    };

    std::atomic_int& number_of_request;
    multiquery_result_t result;
    function_node<par_tbb::msg_result, continue_msg> node;
};

} // namespace par_tbb

#endif // _R2BT_NEIGHBORHOOD_ZMQ_32545034905IJQGWQ3P90JSGDG
