/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_K_QUERY_DISPATCHER_TBB_AI4HRAKFH9QR3298AG
#define _R2BT_K_QUERY_DISPATCHER_TBB_AI4HRAKFH9QR3298AG

#define THIS_FILE get_filename(__FILE__)

namespace par_tbb {

class k_query_dispatcher {
public:
    enum { DIM = 3 };
    typedef std::unordered_map<tessel<DIM>, std::shared_ptr<par_tbb::worker<DIM> > > HashMap;

    k_query_dispatcher( graph& g, const HashMap& h )
        : domain( h )
        , result()
        , node(g, 1, do_work(domain, result, node))
    {
    }

    multi_K_query_result_t multi_k_collect()
    {
        return std::move(result);
    }

    struct do_work
    {
        do_work(const HashMap& d, multi_K_query_result_t& r, function_node<par_tbb::msg_K_data_and_result, continue_msg> &n)
            : domain( d )
            , result(r)
            , node(n)
        {
        }

        static inline double radius_limit( const std::shared_ptr< kquery_result >& result, size_t K, const double& radius )
        {
            return result->size() < K ? std::numeric_limits<double>::infinity() : radius;
        }

        continue_msg operator() (const par_tbb::msg_K_data_and_result &msg) const
        {
            if ( msg.ite != msg.end )
            {
                LOG_OBJ("kquery do work tries to find a cell" << std::endl );
                par_tbb::msg_K_data_and_result res = msg;
                const std::array<int, DIM>& increment = *(msg.ite);
                ++(res.ite);
                if ( msg.mc.filter(msg.point, sqrt(radius_limit(msg.result, msg.K, msg.sq_radius)), msg.center, increment) )
                {
                    res.keep_going = true;
                    tessel<DIM> aux = msg.center + increment;
                    typename HashMap::const_iterator found = domain.find(aux);
                    if ( found != domain.end() )
                    {
                        LOG_OBJ("kquery do work launchs kquery" << std::endl );
                        found->second->node_for_k_query.try_put(res);
                    }
                    else
                    {
                        // Try again
                        node.try_put(res);
                    }
                }
                else
                {
                    // Try again
                    node.try_put(res);
                }
            }
            else if ( msg.keep_going )
            {
                LOG_OBJ("kquery do work computes another increment" << std::endl );
                par_tbb::msg_K_data_and_result res = msg;
                ++(res.r);
                const std::forward_list < std::array<int, DIM> >& increment_list = multi_cell<DIM>::near_cells_3(res.r);
                res.ite = increment_list.cbegin();
                res.end = increment_list.cend();
                res.keep_going = false;
                node.try_put(res);
            }
            else
            {
                LOG_OBJ("kquery do work stores result" << std::endl );
                typename multi_K_query_result_t::iterator found = result.find(msg.address);
                if ( found != result.end() )
                {
                    LOG_OBJ("Error should not be possible to get here in " << THIS_FILE << std::endl);
                }
                else
                {
                    LOG("Add result to new point %p\n", msg.address);
                    result[msg.address] = std::move( *msg.result );
                }
            }
            return continue_msg();
        }

        const HashMap& domain;
        multi_K_query_result_t& result;
        function_node<par_tbb::msg_K_data_and_result, continue_msg> &node;
    };

    const HashMap& domain;
    multi_K_query_result_t result;
    function_node<par_tbb::msg_K_data_and_result, continue_msg> node;
};

} // namespace par_tbb

#endif // _R2BT_K_QUERY_DISPATCHER_TBB_AI4HRAKFH9QR3298AG
