/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _EXAMPLE_POINT_9304q9utw9340qutq3409
#define _EXAMPLE_POINT_9304q9utw9340qutq3409

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>

namespace r2bt {

static int next_label = 0;

template < unsigned DIM >
struct example_point : public std::array<double, DIM>
{
    int label;
    int number;
    static double range;

    example_point()
        : label(next_label)
        , number(0)
    {
        unsigned i;
        for ( i = 0; i < DIM; ++i )
        {
            (*this)[i] = rand() * range / RAND_MAX;
        }
        ++next_label;
    }

    example_point(const double* w, int num = 0)
        : label(next_label)
        , number(num)
    {
        memcpy(this->data(), w, DIM*sizeof(double));
        ++next_label;
    }

    inline bool operator< (const example_point& rhs) const
    {
        return label < rhs.label;
    }

    inline bool near2(const example_point& v2, double dist2) const
    {
        double aux = 0; int i;
        for ( i = 0; i < DIM; ++i )
        {
            aux += ( (*this)[i] - v2[i] ) * ( (*this)[i] - v2[i] );
        }
        return aux < dist2;
    }

    inline std::array<double, DIM> get_coordinates() const
    {
        return std::array<double, DIM>(*this);
    }

    inline void apply( const example_point& p )
    {
        this->number += p.label;
    }

    inline void apply( const example_point* p )
    {
        this->number += p->label;
    }
};

template < unsigned DIM >
inline std::ostream& operator<< (std::ostream& out, const example_point<DIM>& p)
{
    out << " label: " << p.label << ", number: " << p.number <<", ( ";
    unsigned i;
    for ( i = 0; i < DIM; ++i)
    {
        out << p[i] << " ";
    }
    out << " ) ";
    return out;
}

template < unsigned DIM >
inline double distance(const example_point<DIM>& v1, const example_point<DIM>& v2)
{
    double aux = 0;
    unsigned i;
    for ( i = 0; i < DIM; ++i )
    {
        aux += ( v1[i] - v2[i] ) * ( v1[i] - v2[i] );
    }
    return sqrt(aux);
}

template < unsigned DIM >
inline std::array<double, DIM> example2coordinates(const example_point<DIM>& v)
{
    std::array<double, DIM> result;
    unsigned i;
    for ( i = 0; i < DIM; ++i )
    {
        result[i] = v[i];
    }
    return result;
}


} // namespace

#endif // _EXAMPLE_POINT_9304q9utw9340qutq3409
