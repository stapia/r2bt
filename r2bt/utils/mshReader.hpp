/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef MSH_READER_6484643175546431757
#define MSH_READER_6484643175546431757

#include "example_point.hpp"

#include <vector>
#include <cstdio>
#include <cstring>

namespace r2bt {

template < unsigned DIM >
int read_vector( FILE* file, std::vector< example_point<DIM> >& v)
{
    char line[256];
    long unsigned i = 0, j;

    while ( fgets(line, 256, file) && (strcmp(line, "$Nodes\n") != 0) && (i < 10) ) {
         //fprintf(stderr, "Skipping: '%s'\n", line);
         ++i;
    }

    long unsigned size;
    if ( fscanf(file, "%lu ", &size) != 1 ) {
        fprintf(stderr, "Unable to read number of points\n");
        return 1;
    }
    fprintf(stderr, "Number of points: %lu\n", size);

    v.reserve(size);
    double a_point[3];
    i = 0;

    while ( fgets(line, 256, file) && (strcmp(line, "$EndNodes\n") != 0) && (i <= size) ) {

        ++i;

        if ( sscanf(line, "%lu %lf %lf %lf", &j, a_point, a_point+1, a_point+2) != 4 ) {
            fprintf(stderr, "Error: Unexpected format in point %lu, could not read the point\n", i);
            fprintf(stderr, "Line was: '%s'\n", line);
            return 0;
        }

        if ( i != j ) {
            fprintf(stderr, "Error: Unexpected number of point in point %lu, find number %lu\n", i, j);
            fprintf(stderr, "Line was: '%s'\n", line);
            return 0;
        }

        v.push_back( example_point<DIM>(a_point, i) );
    }

    if ( j != size ) {
        fprintf(stderr, "Error: Unexpected number of points, expected %lu, found %lu\n", size, j);
    }
    else {
        fprintf(stderr, "Success: %lu points read from file\n", size);
    }

    return 0;
}

void load_msh_file(const char *filename, std::vector< example_point<3> >& v)
{
    FILE *file = fopen( filename, "r");
    if ( file != NULL ) {
        read_vector(file, v);
        fclose(file);
    }
    else {
        fprintf(stderr, "Can't find: '%s'\n", filename);
    }
}

} //name space

#endif // MSH_READER_6484643175546431757

