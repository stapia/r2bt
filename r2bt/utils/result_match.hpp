/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_RESULT_MATCH_0892735429572395
#define _R2BT_RESULT_MATCH_0892735429572395

namespace r2bt
{

MATCH_RESULT matching( multiquery_result_t& lhs, multiquery_result_t& rhs )
{
    for ( auto& pair : lhs )
    {
        auto found = rhs.find(pair.first);

        if ( found != rhs.end() )
        {
            auto match = matching_results(pair.second, found->second);
            switch ( match ) {
            case EQUAL_RESULT:
                break;
            case MISMATCH:
                LOG_OBJ("Results are different for point " << pair.first << "\n");
                return MISMATCH;
                break;
            case SIZE_MISMATCH:
                LOG_OBJ("Results have different size for point " << pair.first << "\n");
                return SIZE_MISMATCH;
                break;
            default:
                break;
            }
        }
        else
        {
            LOG_OBJ("Query point for first result is not found in second result\n");
            return SIZE_MISMATCH;
        }
    }
    return EQUAL_RESULT;
}


} // namespace

#endif // _R2BT_RESULT_MATCH_0892735429572395
