/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_TEST_POINT_AFJASOFHQ92348RH
#define _R2BT_TEST_POINT_AFJASOFHQ92348RH

namespace r2bt {

class test_point
{
public:
    double v[3];
    int data;
};

inline std::array<double, 3> test_point2coordinates(const test_point& p)
{
    return std::array<double, 3>( { p.v[0], p.v[1], p.v[2] } );
}

inline std::array<double, 3> test_point_ptr2coordinates(const test_point* p)
{
    return std::array<double, 3>( { p->v[0], p->v[1], p->v[2] } );
}

inline std::ostream& operator<< (std::ostream& out, const test_point& p)
{
    out << " data: " << p.data << ", ( ";
        int i;
        for ( i = 0; i < 3; ++i)
        {
                out << p.v[i] << " ";
        }
        out << " ) ";
    return out;
}

} // name space

#endif
