
function createFromVTU(data, status) {
  console.log("load VTU:" + data.substring(0, 100));
  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(data, "text/xml");
  // const text = xmlDoc.getElementsByTagName("title")[0].childNodes[0].nodeValue;
  const piece = xmlDoc.getElementsByTagName("Piece")[0];
  const numPoints = parseInt(piece.getAttribute("NumberOfPoints"));
  console.log("Points Number: " + numPoints);

  const pointsDataArray = xmlDoc.getElementsByTagName("DataArray")[0];
  const numberOfComponents = parseInt(pointsDataArray.getAttribute("NumberOfComponents"));
  console.log("NumberOfComponents: " + numberOfComponents);
  console.log(pointsDataArray.innerHTML.substring(0, 100));

  const geometry = new THREE.BufferGeometry();
  const positions = new Float32Array(numPoints * 3);

  // const coords = pointsDataArray.innerHTML.split("\n");
  const coords = pointsDataArray.innerHTML.match(/[^ \n\t]+/g);
  console.log(coords.length);
  coords.forEach(function (coord, i) {
    positions[i] = parseFloat(coord);
  });

  geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));
  geometry.computeBoundingBox();

  const pointSize = 0.02;
  const material = new THREE.PointsMaterial({ size: pointSize, color: 0x0000FF });

  const obj = new THREE.Points(geometry, material);
  obj.scale.set(25, 25, 25);

  return obj;
}

function createCamera() {
  const fov = 75;
  const aspect = 2;  // the canvas default
  const near = 0.1;
  const far = 100;

  const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

  camera.position.z = 10;

  return camera;
}

function createLight() {
  const color = 0xFFFFFF;
  const intensity = 1;
  const light = new THREE.DirectionalLight(color, intensity);
  light.position.set(-1, 2, 4);
  return light;
}

function resizeRendererToDisplaySize(renderer) {
  const canvas = renderer.domElement;
  const width = canvas.clientWidth;
  const height = canvas.clientHeight;
  const needResize = canvas.width !== width || canvas.height !== height;
  if (needResize) {
    renderer.setSize(width, height, false);
  }
  return needResize;
}

function main() {
  const canvas = document.querySelector('#the_canvas');
  if (canvas === null) {
    console.log('Placeholder not found');
    return;
  }
  const renderer = new THREE.WebGLRenderer({ canvas });

  const scene = new THREE.Scene();
  const camera = createCamera();
  const light = createLight();
  scene.add(light);

  const objects = [];

  function insertInstance(object, x) {
    object.position.x = x;
    scene.add(object);
    objects.push(object);
  }

  function render(time) {
    time *= 0.0001;

    if (resizeRendererToDisplaySize(renderer)) {
      const canvas = renderer.domElement;
      camera.aspect = canvas.clientWidth / canvas.clientHeight;
      camera.updateProjectionMatrix();
    }

    objects.forEach((obj, ndx) => {
      const speed = 1 + ndx * .1;
      const rot = time * speed;
      obj.rotation.x = rot;
      obj.rotation.y = rot;
    });

    renderer.render(scene, camera);

    requestAnimationFrame(render);
  }

  requestAnimationFrame(render);

  return insertInstance;
}

$(document).ready(function () {

  var insertInstance = main();

  $.get(
    "https://bitbucket.org/stapia/r2bt/raw/415518f823e7918ede2796e614dc8bfb81876683/test-data/spring-fem.vtu",
    function (data, status) {
      const obj = createFromVTU(data, status);
      insertInstance(obj, 0.0);
    });
});
