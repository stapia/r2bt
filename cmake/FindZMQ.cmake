# This module finds ZMQ
# !!!!!!!!!!!!!! This is a dummy, not a real implmentaion !!!!!!!!!!!!!!!!!
#
# It sets the following variables:
#  ZMQ_FOUND              - Set to ___false____, or undefined
#  ZMQ_INCLUDE_DIR        - include directory.
#  ZMQ_LIBRARIES          - library files

# ZMQ is temporaly off
set ( ZMQ_FOUND OFF CACHE BOOL "Enable parallelization with ZMQ" )
set ( ZMQ_INCLUDE_DIR ""    CACHE PATH "Path to ZMQ library.")
set ( ZMQ_LIBRARIES   "zmq" CACHE PATH "Path to ZMQ library.")
