/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include <iostream>
#include <r2bt/r2bt.hpp>

int main(int argc, char** argv)
{
    if ( argc != 3 ) {
        std::cerr << "usage: " << argv[0] << " range num" << std::endl;
        std::cerr << "being range the size of the cell (in 2^range) and num the number to locate" << std::endl;
        return 1;
    }

    union number {
        double re;
        uint64_t i;
    } n;

    int64_t lattice;
    int range;

    range = atoi(argv[1]);
    n.re = atof(argv[2]);

    lattice = r2bt::bit_lattice(n.re, range);
    std::cout << "lattice: " << lattice << ";" << std::endl;

    return 0;
}
