/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <r2bt/r2bt.hpp>
#include <r2bt/utils/example_point.hpp>
#include <r2bt/utils/xmlReader.hpp>
#include <r2bt/utils/cell_debugs.hpp>
#include <r2bt/utils/result_match.hpp>

#include "program_options.hpp"

using namespace r2bt;

int main(int argc, char** argv)
{
    program_options options;
    int option_error = options.load(argc, argv);
    if ( option_error ) return option_error;

    std::vector< example_point<3> > searching;
    std::vector< example_point<3> > querying;

    load_file( options.searching_file, searching);
    load_file( options.querying_file,  querying);

    auto neigh1 = factory::createUniquePtr< 3, example_point<3> >(options.ss_type, options.delta, options.cell_type);
    auto neigh2 = factory::createUniquePtr< 3, example_point<3> >("one_cell",      options.delta, "brute");

    insert( neigh1.get(), searching.begin(), searching.end(), example2coordinates<3>);
    insert( neigh2.get(), searching.begin(), searching.end(), example2coordinates<3>);

    auto result1 = query( neigh1.get(), querying.begin(), querying.end(), querying.size(), example2coordinates<3>, options.radius );

    {
        double average = 0.0;

        for ( size_t i = 0; i < querying.size(); ++i )
        {
            auto& v = result1[ i ];
            average += v.size();
        }
        average /= querying.size();
        std::cout << "Average number of neighbors: " << average << std::endl;
    }

    auto result2 = query( neigh2.get(), querying.begin(), querying.end(), querying.size(), example2coordinates<3>, options.radius );
    {
        double average = 0.0;

        for ( size_t i = 0; i < querying.size(); ++i )
        {
            auto& v = result2[ i ];
            average += v.size();
        }
        average /= querying.size();
        std::cout << "Average number of neighbors: " << average << std::endl;
    }

    auto compare = matching(result1, result2);

    switch (compare) {
    case EQUAL_RESULT:
        printf("OK\n");
        break;
    case SIZE_MISMATCH:
        printf("Size mismatch\n");
        break;
    case MISMATCH:
        printf("Result mismatch\n");
        break;
    default:
        break;
    }

    return 0;
}
