#/****************************************************************************
#* (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
#*                    Pablo Hiroshi Alonso-Miyazaki (UPM)
#*
#* This file is part of R2BT Library.
#*
#*     R2BT Library is free software: you can redistribute it and/or modify
#*     it under the terms of the GNU General Public License as published by
#*     the Free Software Foundation, either version 3 of the License, or
#*     (at your option) any later version.
#*
#*     R2BT Library is distributed in the hope that it will be useful,
#*     but WITHOUT ANY WARRANTY; without even the implied warranty of
#*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#*     GNU General Public License for more details.
#*
#*     You should have received a copy of the GNU General Public License
#*     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
#*****************************************************************************/

cmake_minimum_required (VERSION 2.8)

project(r2bt_test)

# Config header file
configure_file ( "${PROJECT_SOURCE_DIR}/config.h.in" "${PROJECT_BINARY_DIR}/config.h" )
include_directories("${PROJECT_BINARY_DIR}")

# Required dependency (for loading data files for testing)
find_package(LibXml2 REQUIRED)
include_directories(${LIBXML2_INCLUDE_DIR})

# Optional libraries
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/../cmake")
include ( FindAll )

enable_testing()

include_directories("..")

set(r2bt_include_dir "../r2bt")
file (GLOB_RECURSE r2bt_header_files RELATIVE "${PROJECT_SOURCE_DIR}" "${r2bt_include_dir}/*.hpp")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")
# Optional settings for CMake
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++11 -Wall -O3 -msse2 -ftree-vectorizer-verbose=1")

################################################################
#
#                   Build targets
#
################################################################

add_executable( r2bt_tester tester.cpp program_options.hpp program_options.cpp ${r2bt_header_files} )
target_link_libraries(r2bt_tester ${LIBXML2_LIBRARIES})
target_link_optional_libs( r2bt_tester )

add_executable( bit_lattice_tester bit_lattice_tester.cpp ${SRC_FILES})

add_executable( near_cell_tester near_cell_tester.cpp ${SRC_FILES})

#Build a testing target

set ( TestNumber 1 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 12.0 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 1;")

set ( TestNumber 2 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 8.000001 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 1;")

set ( TestNumber 3 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 7.999999 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 0;")

set ( TestNumber 4 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 0.00001 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 0;")

set ( TestNumber 5 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 1e-1074 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 0;")

set ( TestNumber 6 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 -0.0001 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: -1;")

set ( TestNumber 7 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 -7.9999 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: -1;")

set ( TestNumber 8 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 -31.9999 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: -4;")

set ( TestNumber 9 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 -32.0 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: -5;")

set ( TestNumber 10 )
add_test( Lattice${TestNumber} bit_lattice_tester 3 48.0 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 6;")

set ( TestNumber 11 )
add_test( Lattice${TestNumber} bit_lattice_tester -2 0.25 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 1;")

set ( TestNumber 12 )
add_test( Lattice${TestNumber} bit_lattice_tester -2 0.250001 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 1;")

set ( TestNumber 13 )
add_test( Lattice${TestNumber} bit_lattice_tester -2 0.249999 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: 0;")

set ( TestNumber 14 )
add_test( Lattice${TestNumber} bit_lattice_tester -2 -0.249999 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: -1;")

set ( TestNumber 15 )
add_test( Lattice${TestNumber} bit_lattice_tester -2 -0.75 )
set_tests_properties(Lattice${TestNumber} PROPERTIES PASS_REGULAR_EXPRESSION "lattice: -4;")
