/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 * 
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include <iostream>

#include <r2bt/par_cell.hpp>
#include <r2bt/brute_cell.hpp>
#include <r2bt/neighborhood_imp.hpp>
#include <r2bt/point_adapter.hpp>
#include <r2bt/point.hpp>
#include <r2bt/xmlReader.hpp>

int main(int argc, char**argv)
{
    double radius = 0.80;
    if ( argc == 2 )
    {
        radius = atof(argv[1]);
    }

    int range = -2; // the cells will be sized 2 raised to -1
    cell<3>* a_cell = new brute_cell<3>;

    neighborhood_imp< 3, point_adapter<3, Point<3> > > neigh(a_cell, range);

	const char* filename = "data/ex1/iris-spots.vtu";
	//const char* filename = "data/ex2/evalpoints.vtu";
	//const char* filename = "data/ex3/gear-spots.vtu";

	std::vector< Point<3> > searching;
	point_loader( searching, filename );


	filename = "data/ex1/iris-fem.vtu";
	//filename = "data/ex2/fem.vtu";
	//filename = "data/ex3/gear-fem.vtu";

	std::vector< Point<3> > querying;
	point_loader( querying, filename );

    size_t len = searching.size();

    std::cout << "Inserting " << len << " points" << std::endl;

    insert( neigh, searching.begin(), searching.end() );

	size_t max = querying.size();

	std::cout << "Searching for " << max << " points" << std::endl;

	for(unsigned j = 0; j < 1; j++){
		point_adapter<3, Point<3> > p(&querying[j]);
		auto v = neigh.query(p, radius);
		std::cout << std::endl << "Neighbors of : " << querying[j] << std::endl;
        for ( auto x : v ) {
            Point<3> * px = (Point<3>*) x;
            std::cout << x << "\t" << *px << std::endl;
        }
	}

    std::cout << "End of program" << std::endl;

    return 0;
}
