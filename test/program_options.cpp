/****************************************************************************
 * (c) 2013 Copyright Santiago Tapia-Fernández (UPM)
 *
 * This file is part of Santy Library.
 *
 *     Santy Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Santy Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Santy Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "program_options.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

int program_options::load(int argc, char** argv)
{
    verbose = 0;
    help = 0;

    random_seed = 4534;
    box = 0;
    box_size = 0.0/0.0; /* NaN */

    radius = 0.5;
    delta = 8.1;
    k = 20;

    cell_type = NULL;
    ss_type = NULL;
    searching_file = NULL;
    querying_file = NULL;

    struct option my_long_options[] =
    {
        /* These options set a flag. */
        {"verbose", no_argument, &verbose, 1},
        {"box",     no_argument, &box,     1},
        {"help",    no_argument, &help,    1},
        //{"brief",   no_argument,       &verbose_flag, 0},
        /* These options don’t set a flag.
         * We distinguish them by their indices. */
        {"delta",      required_argument, 0, 'd' },
        {"radius",     required_argument, 0, 'r' },
        {"K",          required_argument, 0, 'k' },
        {"seed",       required_argument, 0, 's' },
        {0, 0, 0, 0}
    };

    int option_index = 0;
    int c;

    while ( (c = getopt_long (argc, argv, "d:r:k:s:", my_long_options, &option_index) ) != -1 )
    {
        switch (c)
        {
        case 0:
            /* If this option set a flag, do nothing else now. */
            if (my_long_options[option_index].flag != 0)
            {
                break;
            }
            printf ("option %s", my_long_options[option_index].name);
            if (optarg)
            {
                printf (" with arg %s", optarg);
            }
            printf ("\n");
            break;

        case 's':
            random_seed = atoi(optarg);
            printf ("random seed set to %i\n", random_seed);
            break;

        case 'd':
            delta = atof(optarg);
            printf ("delta set to %f\n", delta);
            break;

        case 'r':
            radius = atof(optarg);
            printf ("radius set to %f\n", radius);
            break;

        case 'k':
            k = atoi(optarg);
            printf ("K set to %i\n", k);
            break;

        case '?':
            /* getopt_long already printed an error message. */
            break;

        default:
            return 1;
        }
    }

    if ( help )
    {
        fprintf (stderr,
            "usage: %s [options] ss_type cell_type [ box_size | searching_file querying_file ]\n"
            "\twhere: \n"
            "\t ss_type is the searching set object type, one of: serial, serial_rec, tbb, zmq\n"
            "\t cell_type is the cell object type, one of: stree, utree, brute, omp, ann\n"
            "\t if box option is set you have to provide a box to generate random points in it (TODO)\n"
            "\t searching_file is the filename for the searching set of points\n"
            "\t querying_file is the filename for the querying set of points\n"
            "\n",
            argv[0]);
        return 1;
    }

    /* Arguments are: ss_type, cell_type, box | searching_file, box | querying_file */
    if ( optind + 4 == argc )
    {
        ss_type = argv[optind++];
        cell_type = argv[optind++];
        searching_file = argv[optind++];
        querying_file = argv[optind++];
        printf("Set ss_type, cell_type, searching_file and querying_file as: \n"
               "\t%s, %s, %s, %s\n", ss_type, cell_type, searching_file, querying_file);
    }
    else if ( box && (optind + 3 == argc) )
    {
        ss_type = argv[optind++];
        cell_type = argv[optind++];
        box_size = atof(argv[optind++]);
    }
    else
    {
        fprintf (stderr, "bad arguments:\n"
                 "\tshould be: ss_type cell_type [ box_size | searching_file querying_file ]");
        return 2;
    }
    return 0;
}


