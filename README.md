# README #

## Overview ##

This is the repository for r2bt library. It consists of several algorithm for solving the neighbourhood problem in large set of points.

The library basic algorithms is a "header only" library, just download it and include "r2bt.h", it only depends on standard C++ 
header files, but you will need a *C++ 11 compiler*. In order to provide parallel algorithms the library depends on third party software,
for instance: tbb or OpenMP, but they are not included in "r2bt.h", unless you set a certain macro. 

We have been doing a lot of work in testing and benchmarking in the library, actually, we have submitted an article about the theorical aspects of the
library and a lot of benchmarking. Thus, we estimate that the library is now quite stable (version 1.0), we are documenting, writing examples, tutorial and
tidy up somethings. But interfaces and implementations are not going to change significantly.

## Documentation ##

Please, find the [wiki](https://bitbucket.org/stapia/r2bt/wiki/Home) in this same repository and the Web page at [r2bt](http://simula.industriales.upm.es/software/codes/r2bt.html).
