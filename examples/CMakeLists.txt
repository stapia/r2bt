#/****************************************************************************
#* (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
#*                    Pablo Hiroshi Alonso-Miyazaki (UPM)
#*
#* This file is part of R2BT Library.
#*
#*     R2BT Library is free software: you can redistribute it and/or modify
#*     it under the terms of the GNU General Public License as published by
#*     the Free Software Foundation, either version 3 of the License, or
#*     (at your option) any later version.
#*
#*     R2BT Library is distributed in the hope that it will be useful,
#*     but WITHOUT ANY WARRANTY; without even the implied warranty of
#*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#*     GNU General Public License for more details.
#*
#*     You should have received a copy of the GNU General Public License
#*     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
#*****************************************************************************/

cmake_minimum_required (VERSION 2.8)

#----------------------------------------
#        project name

project(r2bt_examples)

include_directories("..")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")

#------------------------------------------
#        Build targets

add_executable( example example.cpp ${SRC_FILES})
add_executable( example_ptr example_ptr.cpp ${SRC_FILES})

#------------------------------------------
#        Tutorials

find_package(LibXml2)
if(LIBXML2_FOUND)

    include_directories(${LIBXML2_INCLUDE_DIR})

    # Tutorial 1 depends on libxml
    add_executable( 01_tutorial 01_tutorial.cpp)

    target_link_libraries(01_tutorial ${LIBXML2_LIBRARIES})

endif()
