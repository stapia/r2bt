/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include <iostream>

#include <r2bt/r2bt.hpp>
#include <r2bt/utils/test_point.hpp>

using namespace r2bt;

int main(int argc, char** argv)
{
    double h = 3.0;
    auto neigh = ss_factory< 3, test_point >::create ("serial", h, "stree");

    test_point searching[] = {
        { {  2.00, 2.2, 4.3 }, 3 },
        { { 11.00, 2.2, 4.3 }, 4 },
        { {  8.10, 2.2, 4.3 }, 5 },
        { {  7.90, 2.2, 4.3 }, 6 },
        { {  7.91, 2.2, 4.3 }, 7 },
        { {  7.92, 2.2, 4.3 }, 8 },
    };
    size_t len = sizeof( searching ) / sizeof(test_point);

    std::cout << "Inserting " << len << " points" << std::endl;

    insert( *neigh, searching, searching + len, test_point2coordinates );

    // group querying
    if ( 1 ) {
        test_point querying[2] = {
                { { 2.10, 2.2, 4.3 }, 1 },
                { { 8.01, 2.2, 4.3 }, 2 },
            };
        multiquery_result_t r_map = query(*neigh, querying, querying+2, test_point2coordinates, 0.3);

        for ( multiquery_result_t::value_type& x : r_map ) {
            std::cout << std::endl;
            test_point *px = (test_point*) x.first;
            std::cout << " -> " << x.second.size() << " neighbors for point ";
            printf("%lx: ", x.first);
            std::cout << *px << std::endl;

            for ( uintptr_t& vp : x.second )
            {
                test_point *px = (test_point*)vp;
                printf("\t %lx: ", vp);
                std::cout << "\t" << *px << std::endl;
            }
        }
    }

    std::cout << "End of program, then cleaning up" << std::endl;

    return 0;
}
