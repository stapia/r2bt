/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include <iostream>
#include <list>

#include <r2bt/r2bt.hpp>
#include <r2bt/utils/test_point.hpp>
#include <r2bt/utils/example_point.hpp>

using namespace r2bt;

// Implementation detail for one of the class examples
// points will be randomly generated in [ 0, 20.0 ]:
template <unsigned DIM>
double example_point<DIM>::range = 20.0;

int main(int argc, char** argv)
{
    /* This value suggests the cell size in the searching_sets
       Please note that the value should be greater than the average
       radius in the queries
    */
    double h = 8.0;

    /* This is a container for storing the points to be searched looking
       for neighbors. test_point is just a dummy class to show the
       functionality. searching_set_rec is a container that stores
       points in R^3 in some cells and uses a serial algorithm for find
       neighbors.
    */

    auto neigh = factory::createUniquePtr< 3, example_point<3> >("Serial", h, "stree");

    /* You may use any container for manage your points, for instance
     * a C-array or a vector. You only need an iterator for that container */

    /* Querying points could be of another type. In this case, we use
       example_point for searching set and test_point for querying set.
       Both classes are dummy point examples. Note that your points need
       a method called get_coordinates to return their coordinates */

    // in this case, we use a vector to store searching points
    std::vector< example_point<3> > searching(40);

    // and a C-array for querying points
    test_point querying[] = {
        { {  6.8, 15.3,  5.3 }, 103 },
        { {  3.3,  8.8, 17.8 }, 104 },
        { { 18.3, 12.5, 14.1 }, 105 },
        { {  8.3,  3.1, 18.0 }, 106 },

        { { -0.01,  9.10,   1.22 }, 303 },
        { { 17.80,  6.97,  -0.01 }, 304 },
        { {  4.85,  2.74,  16.00 }, 305 },
        { {  8.02,  0.21, -15.90 }, 306 },
    };
    size_t len = sizeof( querying ) / sizeof(test_point);

    /* Note: In this example we use a list of pointer */
    std::list<test_point*> querying_list;

    for ( size_t i = 0; i < len; ++i )
    {
        querying_list.push_back( new test_point(querying[i]) );
    }

    // Output examples points:
    if ( 1 )
    {
        std::cout << "Searching " << searching.size() << " points: " << std::endl;
        for ( auto& p : searching ) {
            std::cout << p << std::endl;
        }
        std::cout << "Querying  " << len << " points: " << std::endl;
        for ( auto *p : querying_list ) {
            std::cout << *p << std::endl;
        }
    }

    std::cout << "Inserting searching " << searching.size() << " points in set" << std::endl;

    /* insert is an overload/template global function that adds
       your points into the searching_set
     */
    insert( neigh.get(), searching.begin(), searching.end(), example2coordinates<3> );


    // Querying neighbors in a ball (using its radius).
    if ( 1 )
    {
        std::cout << std::endl << "----- Querying point in a ball -----" << std::endl;
        const double radius = 3.3;
        multiquery_result_t r_map = query(neigh.get(), querying_list.begin(), querying_list.end(), querying_list.size(), test_point_ptr2coordinates, radius);

        /* The query result is an associative container that maps address of
         * querying points to a container of points */
        for ( multiquery_result_t::value_type& x : r_map )
        {
            std::cout << std::endl;
            /* The first attribute in value pair of associate container
             * is the address of the query point, i.e. test_point in our
             * case */
            test_point *px = (test_point*)x.first;
            std::cout << "Neighbors for " << *px << " radius: " << radius << std::endl;

            /* The second attribute in value pair of associate container
             * is another container of indexes or addresses to points in the searching set.
             * In both cases the stored references are uintptr_t. */
            for ( uintptr_t& vp : x.second ) {
                /* Since we provided a random access iterator to insert searching points,
                 * we get the indexes of those points in our vector. */
                std::cout << "\t" << vp << "\t" << searching[vp] << std::endl;
            }
        }
    }

    // Querying K nearest neighbors
    if ( 1 )
    {
        size_t K = 5;
        std::cout << std::endl << "----- Querying K ( " << K << " ) nearest points -----" << std::endl;
        multi_K_query_result_t r_map = kquery(*neigh, K, querying_list.begin(), querying_list.end(), test_point_ptr2coordinates );

        for ( multi_K_query_result_t::value_type& x : r_map )
        {
            std::cout << std::endl;
            /* Since list iterator is not a random access iterator, first attribute in value
             * pair is the address of the test_point. */
            test_point *px = (test_point*)x.first;
            std::cout << "Neighbors for " << *px << std::endl;

            /* The second attribute in value pair of associate container
             * is container of a pair that contains a the index of the neighbor
             * (since the inserter iterator has the random access tag) and the square distance */
            for ( distance_and_ref& dist_ref : x.second ) {
                std::cout << "\t" << searching[dist_ref.address] << " square distance: " << dist_ref.sq_distance << std::endl;
            }
        }
    }

    // Clear list:
    for ( auto *p : querying_list )
    {
        delete p;
    }

    std::cout << "End of program" << std::endl;

    return 0;
}
