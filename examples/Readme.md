
# Getting Start #

This is the example folder for r2bt library.

### Compiling ###

In order to compile the examples you need:

* C++11 compiler
* CMake version 2.8

Since some examples depend on external libraries, you are advised, either to:

1. Find dependencies and install them: you need ZMQ y TBB.
1. Disable the compilation of those examples by uncheck the variables in CMake with "ex" prefix. 

### Reviewing example ###

Please read comments in example.cpp

Some further explanation:

* The only requirement for your points is that they implement a method
to return the point coordinates:

```
        std::array<double, DIM> get_coordinates() const;
```

* Note that the both, searching and querying points are stored in 
the searching set and in result from the query function as pointers by 
means of intptr_t. You may need to make some cast but it should be trivial.
* The searching set only stores internally the pointer (intptr_t) to the
point and a copy of the coordinates. 
* You should be able to use a container of pointers to points for both
searching or querying sets. 
